package com.db.pbc.trxm.rest.chatapi.controller;

import static org.hamcrest.CoreMatchers.startsWith;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.filter.AuthTokenFilter;
import com.db.pbc.trxm.rest.chatapi.service.ILoginService;
import com.db.pbc.trxm.rest.chatapi.service.IUserService;
import com.db.pbc.trxm.rest.chatapi.testUtils.TestUtils;
import com.fasterxml.jackson.core.JsonProcessingException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContextTest.xml" })
@WebAppConfiguration
public class LogoutControllerTest {

    private final String LOGOUT_USER_URL = "/logout";
    private final User DEFAULT_USER = new User("userControllerTest@name.com", "User Name");
    private final String ERROR_MESSAGE = "$.message";
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private IUserService userService;

    @Autowired
    private ILoginService loginService;

    @Autowired
    private TestUtils utils;

    @Resource
    private AuthTokenFilter tokenFilter;

    @Before
    public void setUp() throws IOException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilters(tokenFilter)
                .build();
    }

    @Test
    public void logoutSuccess() throws JsonProcessingException, Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(tokenFilter)
                .build();
        utils.clearStorageFile();
        userService.clearRegister();
    }

    @Test
    public void logoutUserSuccess() throws JsonProcessingException, Exception {
        userService.registerUser(DEFAULT_USER);
        String token = loginService.login(DEFAULT_USER.getEmail());

        mockMvc.perform(post(LOGOUT_USER_URL).accept(MediaType.APPLICATION_JSON_UTF8)
                .header("AuthToken", token)).andExpect(status().isOk());
    }

    @Test
    public void logoutFailUserWithMissingHeaders() throws JsonProcessingException, Exception {
        userService.registerUser(DEFAULT_USER);
        loginService.login(DEFAULT_USER.getEmail());

        mockMvc.perform(post(LOGOUT_USER_URL).accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void logoutFailUserWithWrongAuth() throws JsonProcessingException, Exception {
        userService.registerUser(DEFAULT_USER);
        String token = loginService.login(DEFAULT_USER.getEmail());

        mockMvc.perform(post(LOGOUT_USER_URL).accept(MediaType.APPLICATION_JSON_UTF8)
                .header("AuthToken", token + "_error")).andExpect(status().isUnauthorized())
                .andExpect(
                        jsonPath(ERROR_MESSAGE, startsWith(("Invalid token or session expired"))));
    }

    @After
    public void cleanUp() throws IOException {
        utils.clearStorageFile();
        userService.clearRegister();
    }
}