package com.db.pbc.trxm.rest.chatapi.service;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.db.pbc.trxm.rest.chatapi.bean.ChatRoom;
import com.db.pbc.trxm.rest.chatapi.bean.Message;
import com.db.pbc.trxm.rest.chatapi.bean.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContextTest.xml" })
@WebAppConfiguration
public class ChatRoomServiceTest {
    private static final int EXPECTED_ROOMS_AFTER_DELETE = 1;
    private static final String ROOM_NAME = "Room_1";
    private int room_id;
    private static final User AUTHOR = new User("testuser@email.com", "Test User");


    @Autowired
    IChatRoomService chatRoomService;

    @Autowired
    IMessageService messageService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    @Before
    public void setUp(){
        room_id = chatRoomService.createChatRoom(ROOM_NAME, false);
    }
    
    @Test

    public void getMessagesFromRoomSuccess(){
        Message msg1 = new Message(AUTHOR, "Test message number 1.");
        Message msg2 = new Message(AUTHOR, "Test message number 2.");
        Message msg3 = new Message(AUTHOR, "Test message number 3.");
        ChatRoom room = chatRoomService.getRoomById(room_id);
        room.getMessages().add(msg1);
        room.getMessages().add(msg2);
        room.getMessages().add(msg3);
        List<Message> returnedMessages = chatRoomService.getMessagesFromRoom(room_id);
        assertThat(returnedMessages, hasItem(msg1));
        assertThat(returnedMessages, hasItem(msg2));
        assertThat(returnedMessages, hasItem(msg3));
    }

    @Test
    public void createChatRoomSuccess() {
        assertThat(chatRoomService.getRoomById(room_id), is(not(nullValue())));
    }

    @Test
    public void deleteChatRoomSuccess() {
        chatRoomService.deleteChatRoomById(room_id);
        assertThat(chatRoomService.listAllRooms().size(), is(EXPECTED_ROOMS_AFTER_DELETE));
    }

    @Test
    public void listAllRoomsSuccess() {
        Set<ChatRoom> createdRooms = chatRoomService.listAllRooms();
        assertThat(createdRooms, hasItem(chatRoomService.getRoomById(room_id)));
    }

    @Test
    public void addMessageInvalidRoomFail() {
        Message msg1 = new Message(AUTHOR, "Test message number 1.");
        Message msg2 = new Message(AUTHOR, "Test message number 2.");

        int roomId = -1231515;

        messageService.addMessage(roomId, msg1);
        messageService.addMessage(roomId, msg2);
        List<Message> messages = chatRoomService.getMessagesFromRoom(roomId);

        assertThat(messages, is(nullValue()));
    }

    @Test
    public void addMessageExistentRoomSuccess() {
        Message msg1 = new Message(AUTHOR, "Test message number 1.");
        Message msg2 = new Message(AUTHOR, "Test message number 2.");

        int roomId = 0;

        messageService.addMessage(roomId, msg1);
        messageService.addMessage(roomId, msg2);

        assertThat(msg2.getContent(), is(chatRoomService.getLastMessageContent(roomId)));
    }
    
    @After
    public void cleanUp(){
        chatRoomService.cleanAllRooms();
    }
}
