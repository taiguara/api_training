package com.db.pbc.trxm.rest.chatapi.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.exceptions.DuplicatedUserException;
import com.db.pbc.trxm.rest.chatapi.exceptions.NoUserProvidedException;
import com.db.pbc.trxm.rest.chatapi.testUtils.TestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContextTest.xml" })
@WebAppConfiguration
public class UserServiceTest {

    private final String USER_EMAIL = "email";
    private final String USER_NAME = "DEFAULT_USER";
    private final String USER_EMAIL_NEW = "email@gft.com";
    private final String USER_NAME_NEW = "DEFAULT_USER";
    private User DEFAULT_USER = new User(USER_EMAIL, USER_NAME);

    @Autowired
    private IUserService userService;

    @Autowired
    private TestUtils utils;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void registerUserFailEmailExists() throws Exception {
        userService.registerUser(DEFAULT_USER);

        thrown.expect(DuplicatedUserException.class);
        final DuplicatedUserException exception = new DuplicatedUserException();
        thrown.expectMessage(exception.getMessage());

        final User userWithSameEmail = new User();
        userWithSameEmail.setEmail(USER_EMAIL);

        userService.registerUser(userWithSameEmail);

    }

    @Test
    public void registerUserFailNoUserInfo() {
        thrown.expect(NoUserProvidedException.class);
        final NoUserProvidedException exception = new NoUserProvidedException();
        thrown.expectMessage(exception.getMessage());

        userService.registerUser(null);

    }

    @Test
    public void registerUserSuccessNewUser() {
        userService.registerUser(DEFAULT_USER);

        assertThat(userService.getUserByEmail(DEFAULT_USER.getEmail()).getEmail(),
                is(not(nullValue())));
        assertThat(userService.getUserByEmail(DEFAULT_USER.getEmail()), is(DEFAULT_USER));
    }

    @Test
    public void registerUserSuccessEditUser() {
        userService.registerUser(DEFAULT_USER);
        User newUser = new User(USER_EMAIL_NEW, USER_NAME_NEW);
        userService.editUser(DEFAULT_USER.getEmail(), newUser);

        assertThat(userService.getUserByEmail(USER_EMAIL_NEW), is(newUser));
        assertThat(userService.getUserByEmail(USER_EMAIL), is(nullValue()));
    }

    @After
    public void cleanUp() throws IOException {
        userService.clearRegister();
        utils.clearStorageFile();
    }

}