package com.db.pbc.trxm.rest.chatapi.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.bean.req.RegisterUserJsonRequest;
import com.db.pbc.trxm.rest.chatapi.exceptions.DuplicatedUserException;
import com.db.pbc.trxm.rest.chatapi.exceptions.UserValidationException;
import com.db.pbc.trxm.rest.chatapi.repositories.IUserRegister;
import com.db.pbc.trxm.rest.chatapi.service.ILoginService;
import com.db.pbc.trxm.rest.chatapi.service.IUserService;
import com.db.pbc.trxm.rest.chatapi.testUtils.TestUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContextTest.xml" })
@WebAppConfiguration
public class RegisterUserControllerTest {
    private final String REGISTER_USER_URL = "/registerUser";
    private final String EDIT_USER_URL = "/User";
    private final RegisterUserJsonRequest DEFAULT_USER = new RegisterUserJsonRequest(
            "userControllerTest@name.com", "User Name");
    private final RegisterUserJsonRequest EDIT_DEFAULT_USER = new RegisterUserJsonRequest(
            "edit@name.com", "Edit User Name");
    private final RegisterUserJsonRequest MISSING_EMAIL_USER = new RegisterUserJsonRequest("",
            "User Name");
    private final RegisterUserJsonRequest INVALID_EMAIL_USER = new RegisterUserJsonRequest(
            "userControllerTestnamecom", "User Name");
    private final ObjectMapper MAPPER = new ObjectMapper();
    private final User user = new User(DEFAULT_USER.getEmail(),DEFAULT_USER.getName());
    private final User edituser = new User(EDIT_DEFAULT_USER.getEmail(),DEFAULT_USER.getName());
    
    private MockMvc mockMvc;

    private final String ERROR_MESSAGE = "$.errorMessage";
    private final String ERROR_DETAILS = "$.detailedErrorMessages";

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestUtils utils;

    @Autowired
    private IUserRegister userRegister;
    
    @Autowired
    private IUserService userService;    

    @Autowired
    private ILoginService loginService;    
    
    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void registerUserSuccessNewUser() throws JsonProcessingException, Exception {
        
        mockMvc.perform(post(REGISTER_USER_URL).accept(MediaType.APPLICATION_JSON_UTF8)
                .content(MAPPER.writeValueAsString(DEFAULT_USER))
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isCreated());
    }
    
    @Test
    public void registerUserSuccessEditUser() throws JsonProcessingException, Exception {
        userService.registerUser(user);
        String token = loginService.login(DEFAULT_USER.getEmail());
        
        mockMvc.perform(put(EDIT_USER_URL).accept(MediaType.APPLICATION_JSON_UTF8)
                .header("AuthToken", token)
                .content(MAPPER.writeValueAsString(EDIT_DEFAULT_USER))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
        
        userRegister.getUserByEmail(EDIT_DEFAULT_USER.getEmail());
        assertThat(userService.getUserByEmail(EDIT_DEFAULT_USER.getEmail()), is(edituser));
    }
    
    @Test
    public void registerUserFailDuplicatedEmail() throws JsonProcessingException, Exception {
        DuplicatedUserException expectedException = new DuplicatedUserException();
        User userToRegister = new User(DEFAULT_USER.getEmail(), DEFAULT_USER.getName());
        userRegister.addUser(userToRegister);
        mockMvc.perform(post(REGISTER_USER_URL).accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(MAPPER.writeValueAsString(DEFAULT_USER))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(expectedException.getCode().value()))
                .andExpect(jsonPath(ERROR_MESSAGE, startsWith((expectedException.getMessage()))));
    }

    @Test
    public void registerUserFailUserWithMissingEmail() throws JsonProcessingException, Exception {
        UserValidationException expectedException = new UserValidationException();

        mockMvc.perform(post(REGISTER_USER_URL).accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(MAPPER.writeValueAsString(MISSING_EMAIL_USER))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(expectedException.getCode().value()))
                .andExpect(jsonPath(ERROR_MESSAGE, startsWith((expectedException.getMessage()))));
    }

    @Test
    public void registerUserFailInvalidEmailFormat() throws JsonProcessingException, Exception {
        UserValidationException expectedException = new UserValidationException();
        mockMvc.perform(post(REGISTER_USER_URL).accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(MAPPER.writeValueAsString(INVALID_EMAIL_USER))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(expectedException.getCode().value()))
                .andExpect(jsonPath(ERROR_MESSAGE, startsWith((expectedException.getMessage()))))
                .andExpect(jsonPath(ERROR_DETAILS, Matchers.hasSize(1)));
    }

    @After
    public void cleanUp() throws IOException {
        utils.clearStorageFile();
        userRegister.clearRegister();
    }
}