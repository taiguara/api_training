package com.db.pbc.trxm.rest.chatapi.beans;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.repositories.IUserRegister;
import com.db.pbc.trxm.rest.chatapi.testUtils.TestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContextTest.xml" })
@WebAppConfiguration
public class UserRegisterTest {
    private static final String NOT_REGISTERED_EMAIL = "notRegisteredEmail@test.com";
    private static final String USER_EMAIL = "testEmail@test.com";
    private static final String USER_NAME = "testUserName";
    private static final User USER = new User(USER_EMAIL, USER_NAME);

    @Autowired
    private IUserRegister register;
    @Autowired
    private TestUtils utils;

    @Test
    public void addUserSuccess() {
        register.addUser(USER);
        assertThat(register.getUserByEmail(USER.getEmail()), is(USER));
    }

    @Test
    public void testGetUserByEmailNullNonExistingUser() {
        assertThat(register.getUserByEmail(NOT_REGISTERED_EMAIL), is(nullValue()));
    }

    @Test
    public void listAllUsersSuccess() {
        List<User> usersTest = new ArrayList<User>();
        for (int i = 0; i < 10; i++) {
            User user = new User(USER_EMAIL + i, USER_NAME + i);
            usersTest.add(user);
            register.addUser(user);
        }
        List<User> compare = register.listAllUsers();
        assertThat(compare.size(), is(10));
        for (User user : usersTest) {
            assertThat(usersTest, hasItem(user));
        }
    }
    
    @After
    public void cleanUp() throws IOException {
        register.clearRegister();
        utils.clearStorageFile();
    }
}
