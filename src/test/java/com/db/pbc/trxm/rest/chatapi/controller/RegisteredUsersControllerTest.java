package com.db.pbc.trxm.rest.chatapi.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.bean.resp.RegisteredUsersJsonResponse;
import com.db.pbc.trxm.rest.chatapi.filter.AuthTokenFilter;
import com.db.pbc.trxm.rest.chatapi.repositories.IUserRegister;
import com.db.pbc.trxm.rest.chatapi.service.ILoginService;
import com.db.pbc.trxm.rest.chatapi.service.ISessionService;
import com.db.pbc.trxm.rest.chatapi.service.IUserService;
import com.db.pbc.trxm.rest.chatapi.testUtils.TestUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContextTest.xml" })
@WebAppConfiguration
public class RegisteredUsersControllerTest {

    @Autowired
    private WebApplicationContext wc;
    @Autowired
    private IUserService userService;
    @Autowired
    private IUserRegister userRegister;
    @Autowired
    private ISessionService sessionRegister;
    @Autowired
    private ILoginService loginService;
    @Autowired
    private TestUtils utils;
    @Resource
    private AuthTokenFilter tokenFilter;

    private MockMvc mockMvc;
    private List<User> usersRegistered;
    private User user;
    private final ObjectMapper MAPPER = new ObjectMapper();

    @Before
    public void setup() throws JsonProcessingException, Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wc).addFilters(tokenFilter).build();
        userRegister.clearRegister();
        for (int i = 0; i < 10; i++) {
            user = new User("email" + i, "name" + i);
            userRegister.addUser(user);
        }
    }

    @Test
    public void registeredUsersSuccess() throws Exception {
        usersRegistered = userRegister.listAllUsers();
        loginService.login(usersRegistered.get(0).getEmail());
        final RegisteredUsersJsonResponse JSONresponse = new RegisteredUsersJsonResponse()
                .withUsers(userService.listAllUsers());
        this.mockMvc
                .perform(get("/registeredUsers").accept(MediaType.APPLICATION_JSON_UTF8).header(
                        "AuthToken",
                        sessionRegister.getSessionByUserEmail(usersRegistered.get(0).getEmail())
                                .getToken())
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk()).andExpect(MockMvcResultMatchers.content()
                        .json(MAPPER.writeValueAsString(JSONresponse)));
    }

    @Test
    public void registeredUsersNullTokenSuccess() throws Exception {
        this.mockMvc
                .perform(get("/registeredUsers").accept(MediaType.APPLICATION_JSON_UTF8)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.message").value("Invalid token or session expired"));
    }    


    @After
    public void cleanUp() throws IOException {
        utils.clearStorageFile();
        userRegister.clearRegister();
    }
}
