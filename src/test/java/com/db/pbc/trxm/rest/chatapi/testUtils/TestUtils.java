package com.db.pbc.trxm.rest.chatapi.testUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.stereotype.Component;

@Component
public class TestUtils {
    @Autowired
    private DefaultResourceLoader resourceLoader;
    private String resourcePath;

    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    public void clearStorageFile() throws IOException {
        FileWriter fw = new FileWriter(resourceLoader.getResource(resourcePath).getFile());
        BufferedWriter bufferWriter = new BufferedWriter(fw);
        bufferWriter.write("");
        bufferWriter.close();
        fw.close();
    }
}
