package com.db.pbc.trxm.rest.chatapi.filter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.service.ISessionService;
import com.db.pbc.trxm.rest.chatapi.service.IUserService;
import com.db.pbc.trxm.rest.chatapi.testUtils.TestUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContextTest.xml" })
@WebAppConfiguration
public class EndpointConfigFilterTest {

    private final ObjectMapper MAPPER = new ObjectMapper();
    private final String TEST_USER_NAME = "Marcelo Conilho";
    private final String TEST_USER_EMAIL = "marcelo@gft.com";
    private final String TEST_URL = "/registerUser";
    private MockMvc mockMvc;
    private String token;

    @Autowired
    private WebApplicationContext wc;

    @Resource
    private EndpointConfigFilter endpointFilter;
    @Resource
    private AuthTokenFilter tokenFilter;
    @Autowired
    private IUserService userService;
    @Autowired
    private ISessionService sessionService;
    @Autowired
    private TestUtils utils;

    @Before
    public void setup() throws JsonProcessingException, Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wc)
                .addFilters(tokenFilter, endpointFilter).build();
        User user = new User(TEST_USER_EMAIL, TEST_USER_NAME);
        this.mockMvc.perform(post("/registerUser").accept(MediaType.APPLICATION_JSON_UTF8)
                .content(MAPPER.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON_UTF8));

        ResultActions result = this.mockMvc.perform(post("/login")
                .accept(MediaType.APPLICATION_JSON_UTF8).content(MAPPER.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON_UTF8));
        Map<String, String> response = new HashMap<>();
        this.token = sessionService.getSessionByUserEmail(TEST_USER_EMAIL).getToken();
        response.put("sessionToken", this.token);
        result.andExpect(status().isOk()).andExpect(
                MockMvcResultMatchers.content().json(MAPPER.writeValueAsString(response)));
        userService.clearRegister();
        try {
            utils.clearStorageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void endpointConfigFilterEndpointEnableSuccess()
            throws JsonProcessingException, Exception {
        User user = new User(TEST_USER_EMAIL, TEST_USER_NAME);
        this.mockMvc
                .perform(post(TEST_URL).accept(MediaType.APPLICATION_JSON_UTF8)
                        .content(MAPPER.writeValueAsString(user))
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());
    }
    @Test
    public void endpointConfigFilterEndpointDisableFail()
            throws JsonProcessingException, Exception {
        mockMvc.perform(
                post("/logout").accept(MediaType.APPLICATION_JSON_UTF8).header("AuthToken", token))
                .andExpect(status().isLocked());
    }
    @After
    public void cleanUp() {
        userService.clearRegister();
        try {
            utils.clearStorageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
