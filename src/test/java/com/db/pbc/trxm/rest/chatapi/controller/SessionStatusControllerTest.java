package com.db.pbc.trxm.rest.chatapi.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.threeten.bp.Instant;

import com.db.pbc.trxm.rest.chatapi.bean.Session;
import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.service.ILoginService;
import com.db.pbc.trxm.rest.chatapi.service.ISessionService;
import com.db.pbc.trxm.rest.chatapi.service.IUserService;
import com.fasterxml.jackson.core.JsonProcessingException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContextTest.xml" })
@WebAppConfiguration
public class SessionStatusControllerTest {

    private final User TEST_USER = new User("testuser@gft.com", "Test User");
    private final String INVALID_TOKEN = "abcde123";
    private MockMvc mockMvc;

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private ILoginService loginService;

    @Autowired
    private IUserService userService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() throws IOException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void sessionStatusSuccessActive() throws JsonProcessingException, Exception {
        userService.registerUser(TEST_USER);
        String authToken = loginService.login(TEST_USER.getEmail());

        mockMvc.perform(get("/sessionStatus").header("AuthToken", authToken))
                .andExpect(status().isOk()).andExpect(jsonPath("$.status").value("Active"));
    }

    @Test
    public void sessionStatusSuccessInvalidToken() throws JsonProcessingException, Exception {
        mockMvc.perform(get("/sessionStatus").header("AuthToken", INVALID_TOKEN))
                .andExpect(status().isOk()).andExpect(jsonPath("$.status").value("Expired"));
    }

    @Test
    public void sessionStatusSuccessSessionExpired() throws JsonProcessingException, Exception {

        final Session session = sessionService.createSession(TEST_USER.getEmail());
        final Instant expiredDate = Instant.now().minusSeconds(1);
        session.setExpirationTime(expiredDate);

        mockMvc.perform(get("/sessionStatus").header("AuthToken", session.getToken()))
                .andExpect(status().isOk()).andExpect(jsonPath("$.status").value("Expired"));
    }

    @Test
    public void sessionStatusSuccessEmptySession() throws JsonProcessingException, Exception {

        final Session session = sessionService.createSession(TEST_USER.getEmail());
        session.setToken("");

        mockMvc.perform(get("/sessionStatus").header("AuthToken", session.getToken()))
                .andExpect(status().isOk()).andExpect(jsonPath("$.status").value("Expired"));
    }
}
