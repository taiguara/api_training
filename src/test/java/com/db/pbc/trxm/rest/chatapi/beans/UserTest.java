package com.db.pbc.trxm.rest.chatapi.beans;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.db.pbc.trxm.rest.chatapi.bean.User;

public class UserTest {
    private final String USER_EMAIL1 = "userEmail@gft.com";
    private final String USER_EMAIL2 = "secondUserEmail@gft.com";

    /**
     * Test user identifier
     */
    @Test
    public void usersEqualsSuccessWithSameEmailAndDiffNames() {
        User user1 = new User();
        user1.setEmail(USER_EMAIL1);
        user1.setName("user1");

        User user2 = new User();
        user2.setEmail(USER_EMAIL1);
        user2.setName("user2");

        assertThat(user1, is(user2));
    }

    @Test
    public void usersEqualsFailWithDiffEmails() {
        User user1 = new User();
        user1.setEmail(USER_EMAIL1);
        user1.setName("user");

        User user2 = new User();
        user2.setEmail(USER_EMAIL2);
        user2.setName("user");

        assertThat(user1, not(user2));
    }
}
