package com.db.pbc.trxm.rest.chatapi.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.bean.req.LoginJsonRequest;
import com.db.pbc.trxm.rest.chatapi.exceptions.NotRegisteredException;
import com.db.pbc.trxm.rest.chatapi.exceptions.UserValidationException;
import com.db.pbc.trxm.rest.chatapi.repositories.ISessionRegister;
import com.db.pbc.trxm.rest.chatapi.repositories.IUserRegister;
import com.db.pbc.trxm.rest.chatapi.testUtils.TestUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContextTest.xml" })
@WebAppConfiguration
public class LoginControllerTest {
    private final String TEST_USER_NAME = "Marcelo Conilho Abrantes";
    private final String TEST_USER_EMAIL = "marcelo.abrantes@gft.com";
    private final String TEST_UNREGISTER_EMAIL = "unregistered.user@gft.com";
    private final String ERROR_MESSAGE = "$.errorMessage";
    private final String ERROR_DETAILS = "$.detailedErrorMessages";

    private MockMvc mockMvc;
    private final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ISessionRegister sessionRegister;
    @Autowired
    private IUserRegister userRegister;

    @Autowired
    private TestUtils utils;

    @Before
    public void setup() throws JsonProcessingException, Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void loginSuccessRegisteredUser() throws JsonProcessingException, Exception {
        final User userToRegister = new User(TEST_USER_EMAIL, TEST_USER_NAME);
        userRegister.addUser(userToRegister);
        LoginJsonRequest userToLogin = new LoginJsonRequest(TEST_USER_EMAIL, TEST_USER_NAME);

        final ResultActions result = this.mockMvc
                .perform(post("/login").accept(MediaType.APPLICATION_JSON_UTF8)
                        .content(MAPPER.writeValueAsString(userToLogin))
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        Map<String, String> response = new HashMap<>();
        final String sessionToken = sessionRegister.getSessionByUserEmail(userToLogin.getEmail())
                .getToken();
        response.put("sessionToken", sessionToken);

        result.andExpect(MockMvcResultMatchers.content().json(MAPPER.writeValueAsString(response)));
    }

    @Test
    public void loginFailUserUnauthorized() throws JsonProcessingException, Exception {
        final LoginJsonRequest unregisteredUser = new LoginJsonRequest(TEST_UNREGISTER_EMAIL,
                "unregistered user");

        NotRegisteredException expectedException = new NotRegisteredException();

        this.mockMvc
                .perform(post("/login").accept(MediaType.APPLICATION_JSON_UTF8)
                        .content(MAPPER.writeValueAsString(unregisteredUser))
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(expectedException.getCode().value()))
                .andExpect(jsonPath(ERROR_MESSAGE, is(expectedException.getMessage())));
    }

    @Test
    public void loginFailInvalidEmailFormat() throws JsonProcessingException, Exception {
        String invalidEmailFormat = "invalideMailFormat";
        LoginJsonRequest userToLogin = new LoginJsonRequest(invalidEmailFormat, TEST_USER_NAME);

        UserValidationException expectedException = new UserValidationException();

        this.mockMvc
                .perform(post("/login").accept(MediaType.APPLICATION_JSON_UTF8)
                        .content(MAPPER.writeValueAsString(userToLogin))
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(expectedException.getCode().value()))
                .andExpect(jsonPath(ERROR_MESSAGE, is((expectedException.getMessage()))))
                .andExpect(jsonPath(ERROR_DETAILS, Matchers.hasSize(1)));
    }

    @Test
    public void loginFailMissingUser() throws JsonProcessingException, Exception {
        LoginJsonRequest emptyUser = null;
        this.mockMvc
                .perform(post("/login").accept(MediaType.APPLICATION_JSON_UTF8)
                        .content(MAPPER.writeValueAsString(emptyUser))
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @After
    public void cleanUp() throws IOException {
        utils.clearStorageFile();
        userRegister.clearRegister();
    }

}