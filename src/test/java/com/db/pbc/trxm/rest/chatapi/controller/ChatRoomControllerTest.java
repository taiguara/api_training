package com.db.pbc.trxm.rest.chatapi.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.db.pbc.trxm.rest.chatapi.bean.Message;
import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.bean.resp.MessagesJsonResponse;
import com.db.pbc.trxm.rest.chatapi.filter.AuthTokenFilter;
import com.db.pbc.trxm.rest.chatapi.service.IChatRoomService;
import com.db.pbc.trxm.rest.chatapi.service.ILoginService;
import com.db.pbc.trxm.rest.chatapi.service.IMessageService;
import com.db.pbc.trxm.rest.chatapi.service.IUserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContextTest.xml" })
@WebAppConfiguration
public class ChatRoomControllerTest {
    private static final int FALSE_ID = -500;
    private static final int DEFAULT_ID = 0;
    private static final String URL_PARAMETER_ID = "id";
    private static final String URL_PARAMETER_ROOM_ID = "roomId";
    private static final String URL_GET_MESSAGES_FROM_ROOM = "/chatRoom/messages";
    private static final String URL_POST_ADD_NEW_MESSAGE = "/chatRoom/Message";
    private static final String URL_POST_ADD_NEW_FILE = "/chatRoom/File";
    private static final String URL_POST_ADD_NEW_ROOM = "/Room";

    private static final String MESSAGE_JSON = "{\"id\":\"0\",\"message\":\"Hello Everyone! My name is Jackson, as you can see in the system message that I have just logged in.\"}";
    private static final String MESSAGE_JSON_INVALID_CHANNEL = "{\"id\":\"-1234\",\"message\":\"Message test to test this test.\"}";
    private static final String NEWROOM_JSON = "{\"roomName\":\"NovaSala\",\"publicRoom\":false}";
    private static final String NEWROOM_PUBLIC_JSON = "{\"roomName\":\"NovaSala234\",\"publicRoom\":true}";
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final User DEFAULT_USER = new User("usersample1@email.com", "User Sample1");
    private String token;

    private static final MockMultipartFile file = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
    
    private static MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    IChatRoomService chatRoomService;
    @Autowired
    IMessageService messageService;
    @Autowired
    ILoginService loginService;
    @Autowired
    IUserService userService;

    @Resource
    private AuthTokenFilter tokenFilter;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilters(tokenFilter)
                .build();
        userService.clearRegister();
        userService.registerUser(DEFAULT_USER);
        token = loginService.login(DEFAULT_USER.getEmail());

        Message msg1 = new Message(DEFAULT_USER, "Test message number 1.");
        Message msg2 = new Message(DEFAULT_USER, "Test message number 2.");
        Message msg3 = new Message(DEFAULT_USER, "Test message number 3.");
        messageService.addMessage(DEFAULT_ID, msg1);
        messageService.addMessage(DEFAULT_ID, msg2);
        messageService.addMessage(DEFAULT_ID, msg3);
    }

    @Test
    public void messagesFromRoomSuccess() throws Exception {
        MessagesJsonResponse jsonResponse = new MessagesJsonResponse(
                chatRoomService.getMessagesFromRoom(DEFAULT_ID));

        mockMvc.perform(get(URL_GET_MESSAGES_FROM_ROOM).header("AuthToken", token)
                .param(URL_PARAMETER_ID, String.valueOf(DEFAULT_ID))).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .json(MAPPER.writeValueAsString(jsonResponse)));
    };

    @Test
    public void messagesFromRoomFailNonexistentRoom() throws Exception {
        mockMvc.perform(get(URL_GET_MESSAGES_FROM_ROOM).header("AuthToken", token)
                .param(URL_PARAMETER_ID, String.valueOf(FALSE_ID)))
                .andExpect(status().isNotFound());
    };

    @Test
    public void addMessageSuccess() throws JsonProcessingException, Exception {
        mockMvc.perform(post(URL_POST_ADD_NEW_MESSAGE).accept(MediaType.APPLICATION_JSON_UTF8)
                .header("AuthToken", token).content(MESSAGE_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isCreated());
    }
/*
    @Test
    public void addFileSuccess() throws JsonProcessingException, Exception {
        mockMvc.perform(post(URL_POST_ADD_NEW_FILE).accept(MediaType.APPLICATION_JSON_UTF8)
                .header("AuthToken", token).param(URL_PARAMETER_ROOM_ID, String.valueOf(DEFAULT_ID)).requestAttr("file", file.getBytes())
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)).andExpect(status().isCreated());
    }
*/    
    @Test
    public void addMessageSuccessInvalidRoom() throws JsonProcessingException, Exception {
        mockMvc.perform(post(URL_POST_ADD_NEW_MESSAGE).accept(MediaType.APPLICATION_JSON_UTF8)
                .header("AuthToken", token).content(MESSAGE_JSON_INVALID_CHANNEL)
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isNotFound());
    }

    @Test
    public void addRoomSuccess() throws JsonProcessingException, Exception {
        mockMvc.perform(post(URL_POST_ADD_NEW_ROOM).accept(MediaType.APPLICATION_JSON_UTF8)
                .header("AuthToken", token).content(NEWROOM_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isCreated());
    }

    @Test
    public void addRoomSuccessPublic() throws JsonProcessingException, Exception {

        mockMvc.perform(post(URL_POST_ADD_NEW_ROOM).accept(MediaType.APPLICATION_JSON_UTF8)
                .header("AuthToken", token)
                .content(NEWROOM_PUBLIC_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isCreated());
    }

    @Test
    public void addRoomSuccessPublicSameName() throws JsonProcessingException, Exception {

        mockMvc.perform(post(URL_POST_ADD_NEW_ROOM).accept(MediaType.APPLICATION_JSON_UTF8)
                .header("AuthToken", token)
                .content(NEWROOM_PUBLIC_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isCreated());
    }    
    
    @Test
    public void addRoomSuccessNotLogged() throws JsonProcessingException, Exception {

        mockMvc.perform(post(URL_POST_ADD_NEW_ROOM).accept(MediaType.APPLICATION_JSON_UTF8)
                .header("AuthToken", "").content(NEWROOM_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isUnauthorized());
    }
}
