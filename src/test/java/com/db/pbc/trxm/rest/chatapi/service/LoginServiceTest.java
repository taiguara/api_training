package com.db.pbc.trxm.rest.chatapi.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.threeten.bp.Instant;

import com.db.pbc.trxm.rest.chatapi.bean.Session;
import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.exceptions.NotRegisteredException;
import com.db.pbc.trxm.rest.chatapi.testUtils.TestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContextTest.xml" })
@WebAppConfiguration
public class LoginServiceTest {
    private final String NOT_REGISTERED_EMAIL = "notRegisteredEmail";
    private final String USER_EMAIL = "email";
    private final String USER_NAME = "DEFAULT_USER";
    private User DEFAULT_USER = new User(USER_EMAIL, USER_NAME);

    @Autowired
    private TestUtils utils;

    @Autowired
    private ILoginService loginService;

    @Autowired
    private IUserService userService;

    @Autowired
    private ISessionService sessionService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void loginFailNotRegisteredUser() {
        thrown.expect(NotRegisteredException.class);
        final NotRegisteredException exception = new NotRegisteredException();
        thrown.expectMessage(exception.getMessage());

        loginService.login(NOT_REGISTERED_EMAIL);
    }

    @Test
    public void loginFailRegisteredUserWithoutSession() {
        userService.registerUser(DEFAULT_USER);

        final String authenticationToken = loginService.login(USER_EMAIL);

        assertThat(authenticationToken, is(not(nullValue())));
        assertThat(sessionService.isSessionActive(authenticationToken), is(true));
    }

    @Test
    public void loginSuccessRegisteredUserWithActiveSession() {
        userService.registerUser(DEFAULT_USER);
        final Session activeSession = sessionService.createSession(DEFAULT_USER.getEmail());
        activeSession.setExpirationTime(
                Instant.ofEpochMilli(activeSession.getExpirationTime().toEpochMilli() - 1000L));
        
        final long provisionalDateInMilis = activeSession.getExpirationTime().toEpochMilli();

        final String authenticationToken = loginService.login(DEFAULT_USER.getEmail());
        Session newSession = sessionService.getSessionByToken(authenticationToken);

        assertThat(authenticationToken, is(not(nullValue())));
        assertThat(authenticationToken, is(not(activeSession.getToken())));
        assertThat(newSession.getExpirationTime().toEpochMilli() > provisionalDateInMilis,
                is(true));

    }

    @Test
    public void loginSuccessRegisteredUserWithExpiredSession() {
        userService.registerUser(DEFAULT_USER);

        final Session activeSession = sessionService.createSession(DEFAULT_USER.getEmail());
        activeSession
                .setExpirationTime(Instant.ofEpochMilli(System.currentTimeMillis() - 1000 * 60));

        final String authenticationToken = loginService.login(DEFAULT_USER.getEmail());

        assertThat(authenticationToken, is(not(nullValue())));
        assertThat(authenticationToken, is(not(activeSession.getToken())));
    }

    @After
    public void cleanUp() throws IOException {
        // TODO: see if it makes sense to remove usage of the service!!!
        // there is no need to DEFAULT_USER the actual service for login,
        // maybe it's enough to just mock the information in corresponding
        // register
        userService.clearRegister();
        sessionService.clearRegister();
        utils.clearStorageFile();
    }

}