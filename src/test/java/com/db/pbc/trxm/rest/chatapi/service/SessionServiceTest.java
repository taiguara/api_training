package com.db.pbc.trxm.rest.chatapi.service;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.threeten.bp.Instant;

import com.db.pbc.trxm.rest.chatapi.bean.Session;
import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.testUtils.TestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContextTest.xml" })
@WebAppConfiguration
public class SessionServiceTest {

    private static final String USER_EMAIL = "email";
    private static final String USER2_EMAIL = "email2";
    private static final String USER_NAME = "name";
    private static final String USER2_NAME = "name2";

    private final User user = new User(USER_EMAIL, USER_NAME);
    private final User user2= new User(USER2_EMAIL, USER2_NAME);;

    @Autowired
    IUserService userService;
    @Autowired
    ISessionService sessionService;
    

    @Autowired
    private TestUtils utils;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void userActiveWithSessionSuccess() throws Exception {
        userService.registerUser(user);

        final Session session = sessionService.createSession(user.getEmail());
        session.setExpirationTime(
                Instant.ofEpochMilli(session.getExpirationTime().toEpochMilli() + 1000L));
        assertThat(sessionService.isSessionActive(session.getToken()), is(true));
    }

    @Test
    public void userNotActiveBecauseSessionExpired() throws Exception {
        userService.registerUser(user);

        final Session session = sessionService.createSession(user.getEmail());
        final Instant expiredDate = Instant.ofEpochMilli(Instant.now().toEpochMilli() - 1000L);
        session.setExpirationTime(expiredDate);
        assertThat(sessionService.isSessionActive(session.getToken()), is(false));
    }

    @Test
    public void userIsNotActiveBecauseHasNoSession() throws Exception {
        assertThat(sessionService.isSessionActive("NonExistingToken"), is(false));
    }

    @Test
    public void removeSessionSuccess() {
        userService.registerUser(user);

        final Session session = sessionService.createSession(user.getEmail());
        session.setExpirationTime(
                Instant.ofEpochMilli(session.getExpirationTime().toEpochMilli() + 1000L));

        assertThat(sessionService.isSessionActive(session.getToken()), is(true));

        sessionService.removeSession(session.getToken());

        assertThat(sessionService.isSessionActive(session.getToken()), is(false));
    }

    @Test
    public void listUsersInSessionSuccess(){
        userService.registerUser(user);
        userService.registerUser(user2);
        
        final Session session = sessionService.createSession(user.getEmail());
        assertThat(sessionService.isSessionActive(session.getToken()), is(true));
        
        final Session session2 = sessionService.createSession(user2.getEmail());
        assertThat(sessionService.isSessionActive(session2.getToken()), is(true));
        
        List<User> usersInSession = sessionService.listUsersInSession();
        assertThat(usersInSession, hasItems(user, user2));
    }
    
    @After
    public void cleanUp() throws IOException {
        userService.clearRegister();
        sessionService.clearRegister();
        utils.clearStorageFile();
    }

}