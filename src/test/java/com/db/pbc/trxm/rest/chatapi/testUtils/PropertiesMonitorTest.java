package com.db.pbc.trxm.rest.chatapi.testUtils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.db.pbc.trxm.rest.chatapi.utils.ApplicationProperties;
import com.db.pbc.trxm.rest.chatapi.utils.PropertiesMonitor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContextTest.xml" })
@WebAppConfiguration
public class PropertiesMonitorTest {
    @Mock
    private ApplicationProperties properties;
    private static final String APP_TEST_VAR = "app.test.var";
    private int testProperties;
    private File propertiesFile;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Properties prop = new Properties();
        InputStream in = null;
        FileOutputStream fOut = null;
        try {
            // first load
            ClassLoader classLoader = getClass().getClassLoader();
            propertiesFile = new File(
                    classLoader.getResource("applicationTest.properties").getFile());
            in = new FileInputStream(propertiesFile);
            prop.load(in);
            properties = Mockito.spy(ApplicationProperties.class);
            when(properties.getConfiguration()).thenReturn(prop);
            when(properties.getFilename()).thenReturn(propertiesFile.getAbsolutePath());
            //instantiate the properties monitor.
            new PropertiesMonitor(properties);

            // now change and save
            prop.setProperty(APP_TEST_VAR, "100");
            fOut = new FileOutputStream(propertiesFile);
            prop.store(fOut, "Testing");
            
            // wait for the file change trigger
            Thread.sleep(2000);
            testProperties = properties.getValueAsInteger(APP_TEST_VAR);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null)
                    in.close();
                if (fOut != null)
                    fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    @Test
    public void testReadingProperties() {
        assertThat(testProperties, not(0));
    };

    @Test
    public void testParameterChange() {
        System.out.println("Start with: " + testProperties);
        // change parameter
        Properties changedProp = new Properties();
        InputStream in = null;
        FileOutputStream fOut = null;
        try {
            // first load
            in = new FileInputStream(propertiesFile);
            changedProp.load(in);
            // now change and save
            changedProp.setProperty(APP_TEST_VAR, "200");
            fOut = new FileOutputStream(propertiesFile);
            changedProp.store(fOut, "Testing");
            
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null)
                    in.close();
                if (fOut != null)
                    fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //now wait for reload monitoring and read using application properties
        try {
            Thread.sleep(2000);
            int reloadedValue = properties.getValueAsInteger(APP_TEST_VAR);
            System.out.println("File changed? Before: " + testProperties + " now: "
                    + properties.getValueAsInteger(APP_TEST_VAR));
            assertThat(reloadedValue, is(not(equalTo(testProperties))));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    };
}
