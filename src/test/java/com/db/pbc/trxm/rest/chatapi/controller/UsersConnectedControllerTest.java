package com.db.pbc.trxm.rest.chatapi.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.db.pbc.trxm.rest.chatapi.bean.Session;
import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.bean.resp.UsersConnectedJsonResponse;
import com.db.pbc.trxm.rest.chatapi.filter.AuthTokenFilter;
import com.db.pbc.trxm.rest.chatapi.service.ISessionService;
import com.db.pbc.trxm.rest.chatapi.service.IUserService;
import com.db.pbc.trxm.rest.chatapi.testUtils.TestUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/applicationContextTest.xml"})
@WebAppConfiguration
public class UsersConnectedControllerTest {

    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static MockMvc mockMvc;

    private static final String USER_EMAIL = "email";
    private static final String USER2_EMAIL = "email2";
    private static final String USER_NAME = "name";
    private static final String USER2_NAME = "name2";

    private final User user = new User(USER_EMAIL, USER_NAME);
    private final User user2 = new User(USER2_EMAIL, USER2_NAME);

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Resource
    private AuthTokenFilter authTokenFilter;
    @Autowired
    ISessionService sessionService;
    @Autowired
    IUserService userService;
    
    @Autowired
    private TestUtils utils;
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(authTokenFilter).build();
    }

    @Test
    public void listAllUsersConnectedSuccess() throws JsonProcessingException, Exception {
        userService.registerUser(user);
        userService.registerUser(user2);

        final Session session = sessionService.createSession(user.getEmail());
        assertThat(sessionService.isSessionActive(session.getToken()), is(true));
        final Session session2 = sessionService.createSession(user2.getEmail());
        assertThat(sessionService.isSessionActive(session2.getToken()), is(true));

        UsersConnectedJsonResponse jsonResponse = new UsersConnectedJsonResponse(
                sessionService.listUsersInSession());

        mockMvc.perform(get("/usersConnected")
        .header("AuthToken", session.getToken()))
        .andExpect(status().isOk()).andExpect(
                MockMvcResultMatchers.content().json(MAPPER.writeValueAsString(jsonResponse)));
    }
    
    @Test
    public void listAllUsersConnectedFailCauseIsNotLogged() throws JsonProcessingException, Exception {
        mockMvc.perform(get("/usersConnected").header("AuthToken", "")).andExpect(status().isUnauthorized());
    }

    @After
    public void cleanUp() throws IOException {
        userService.clearRegister();
        sessionService.clearRegister();
        utils.clearStorageFile();
    }
}
