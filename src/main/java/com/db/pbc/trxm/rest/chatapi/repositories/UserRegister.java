package com.db.pbc.trxm.rest.chatapi.repositories;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;

import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.exceptions.ErrorReadingStorageResource;
import com.db.pbc.trxm.rest.chatapi.exceptions.ErrorWritingToStorageResource;
import com.db.pbc.trxm.rest.chatapi.exceptions.ResourceStorageNotFound;
import com.db.pbc.trxm.rest.chatapi.utils.StorageLibrary;

//@Component
public class UserRegister implements IUserRegister {
    @Autowired
    private DefaultResourceLoader resourceLoader;
    /**
     * hashmap to manage the users and sessions
     */
    private static Map<String, User> users = new ConcurrentHashMap<String, User>();
    private String resourcePath;
    private static Resource storage;

    @PostConstruct
    public void initRegister() {
        storage = resourceLoader.getResource(this.resourcePath);

        try {
            if (storage.exists()) {
                StorageLibrary.loadUsersFromStorage(storage, users);
            } else {
                throw new ResourceStorageNotFound();
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new ErrorReadingStorageResource();
        }
    }

    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    /**
     * add user to the userRegister.
     */
    @Override
    public void addUser(final User user) {
        users.put(user.getEmail(), user);
        try {
            StorageLibrary.appendUserData(storage, user);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ErrorWritingToStorageResource();
        }
    }

    /**
     * remove registered user
     */
    @Override
    public void removeUser(final User user) {
        String userMail = user.getEmail();
        users.remove(userMail);
        try {
            StorageLibrary.removeUserData(storage, users, userMail);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ErrorWritingToStorageResource();
        }
    }

    /**
     * find user in the userRegister by the userEmail.
     *
     * @return: User found or null if the provided email is not registered in
     *          the system
     */
    @Override
    public User getUserByEmail(final String email) {
        return users.get(email);
    }

    /**
     * Return a List of User with the Map contents.
     */
    public List<User> listAllUsers() {
        return new ArrayList<User>(users.values());
    }

    /**
     * remove all users from userRegister.
     */
    @Override
    @PreDestroy
    public void clearRegister() {
        users.clear();
    }
}