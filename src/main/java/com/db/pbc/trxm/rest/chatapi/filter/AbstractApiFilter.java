package com.db.pbc.trxm.rest.chatapi.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;

public abstract class AbstractApiFilter implements Filter {

    private List<String> exclude = new ArrayList<String>();

    @Override
    public abstract void init(FilterConfig filterConfig) throws ServletException;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        boolean contain = false;
        for (String excludeURL : exclude) {
            if (req.getRequestURI().contains(excludeURL)) {
                contain = true;
            }
        }
        if (!contain) {
            doInternalFilter(req, res, chain);
        } else {
            chain.doFilter(request, response);
        }
    }

    protected abstract void doInternalFilter(HttpServletRequest req, HttpServletResponse res,
            FilterChain chain) throws JsonProcessingException, IOException, ServletException;

    @Override
    public abstract void destroy();

    public List<String> getExclude() {
        return exclude;
    }

    public void setExclude(List<String> exclude) {
        this.exclude = exclude;
    }

}
