package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

public class ResourceStorageNotFound extends ApiBaseException {
    private static final long serialVersionUID = 1L;
    private static final String STORAGE_RESOURCE_NOT_FOUND_MESSAGE = "Storage resource not found";

    public ResourceStorageNotFound() {
        super(HttpStatus.INTERNAL_SERVER_ERROR, STORAGE_RESOURCE_NOT_FOUND_MESSAGE);
    }

}
