package com.db.pbc.trxm.rest.chatapi.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

import com.db.pbc.trxm.rest.chatapi.bean.Session;

public class ChatLibrary {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static String encodeSession(final Session session) {
        return encode(session.getToken() + ":" + session.getCreationTime());
    }

    public static String encode(final String stringToEncode) {
        final byte[] digest = DigestUtils.sha256(stringToEncode);
        return new String(Base64.encodeBase64(digest));
    }

    public static boolean validateEmail(final String email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);;
        return matcher.matches();

    }
    public static String encodeString(final String stringToEncode) {
        final byte[] digest = DigestUtils.sha256(stringToEncode);
        return new String(Base64.encodeBase64(digest));
    }
}
