package com.db.pbc.trxm.rest.chatapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.db.pbc.trxm.rest.chatapi.exceptions.ApiBaseException;
import com.db.pbc.trxm.rest.chatapi.service.ISessionService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
public class LogoutController {
	@Autowired
	private ISessionService sessionService;

	@ApiOperation(value = "Logout a user", notes = "Execute a logout of the user in the system by removing the related session")
	@RequestMapping(value = "/logout", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Void> logoutUser(
			@ApiParam(value = "Authorization Token", required = true) @RequestHeader(value = "AuthToken") String authToken)
			throws ApiBaseException {

		sessionService.removeSession(authToken);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
