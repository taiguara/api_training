package com.db.pbc.trxm.rest.chatapi.bean.req;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "LoginJsonRequest", description = "Login request Json representation")
public class LoginJsonRequest {
    @ApiModelProperty(value = "Person's e-mail address", required = true)
    @NotEmpty(message = "User email can not be empty")
    @Email(message = "Email format is wrong")
    private String email;
    @ApiModelProperty(value = "User name", required = true)
    @Size(min = 2, max = 32, message = "User name length should be from 2 to 32")
    private String name;

    public LoginJsonRequest(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public LoginJsonRequest() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
