package com.db.pbc.trxm.rest.chatapi.repositories;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.db.pbc.trxm.rest.chatapi.bean.ChatFile;
import com.db.pbc.trxm.rest.chatapi.bean.ChatRoom;
import com.db.pbc.trxm.rest.chatapi.bean.Message;
import com.db.pbc.trxm.rest.chatapi.togglz.MyFeatures;

@Component
public class ChatRoomRegister implements IChatRoomRegister {
    private static final String DEFAULT_ROOM = "Chat_Room_0";
    private static final Pattern DIGIT_PATTERN = Pattern.compile("(\\d+)");
    private static final Integer ID_DEFAULT_ROOM = 0;
    private static int last_id = 0;
    public static final int DEFAULT_FILE_ID = 0;
    private static final Boolean DEFAULT_PUBLIC = true;
    private Map<Integer, ChatRoom> indexIdChatRoom = new ConcurrentHashMap<Integer, ChatRoom>();

    @PostConstruct
    public void initChatRoomRegister() {
        createChatRoom(DEFAULT_ROOM, DEFAULT_PUBLIC);
    }

    @Override
    public int createChatRoom(String name, Boolean isPublic) {
        /**
         * TODO: When create the chatroom by controller, return the name with
         * which the room was created.
         */
        ChatRoom room = new ChatRoom();
        room.setId(last_id);
        room.setName(name);
        room.setPublic(isPublic);
        
        while (indexIdChatRoom.containsValue(room)) {
            Matcher matcher = DIGIT_PATTERN.matcher(room.getName());
            int complement = 0;
            if (matcher.find()) {
                complement = Integer.parseInt(matcher.group());
            }
            room.setName(name.concat(String.valueOf(complement + 1)));
        }
        indexIdChatRoom.put(last_id, room);
        incrementId();
        return room.getId();
    }

    @Override
    public void deleteChatRoom(int id) {
        indexIdChatRoom.remove(id);
    }

    @Override
    public ChatRoom getRoomById(int id) {
        return indexIdChatRoom.get(id);
    }

    @Override
    public Set<ChatRoom> listAllRooms() {
        return new HashSet<ChatRoom>(indexIdChatRoom.values());
    }

    @Override
    public void cleanAllRooms() {
        ChatRoom room = new ChatRoom(this.getRoomById(ID_DEFAULT_ROOM));
        indexIdChatRoom.clear();
        indexIdChatRoom.put(ID_DEFAULT_ROOM, room);
    }

    private static final synchronized void incrementId() {
        last_id++;
    }

    @Override
    public void addMessage(int id, Message message) {
        ChatRoom chatRoom = getRoomById(id);
        if (chatRoom != null) {
            List<Message> messages = chatRoom.getMessages();
            messages.add(message);
        }
    }

    @Override
    public int addFile(int id, ChatFile file) {
        ChatRoom chatRoom = getRoomById(id);
        if (chatRoom != null) {
            List<ChatFile> files = chatRoom.getFiles();
            files.add(file);
            return files.size();
        }
        return DEFAULT_FILE_ID;
    }

    @Override
    public String getLastMessageContent(int id) {
        ChatRoom room = getRoomById(id);
        List<Message> messages = room.getMessages();
        Message lastMsg = messages.get(messages.size() - 1);

        return lastMsg.getContent();
    }

    @Override
    public Boolean isPublic(int id) {
        return getRoomById(id).isPublic();
    }

    @Override
    public ChatFile getFile(int roomId, int fileId) {
        ChatRoom room = getRoomById(roomId);
        ChatFile file = room.getFiles().get(fileId-1);
        return file;
    }

    @Override
    public boolean fileExists(int roomId, int fileId) {
        ChatRoom room = getRoomById(roomId);
        if (room != null) {
            if (fileId <= room.getFiles().size())
                return true;
            else 
                return false;
        }
        return false;
    }
}
