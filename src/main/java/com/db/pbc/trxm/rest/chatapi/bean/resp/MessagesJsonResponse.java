package com.db.pbc.trxm.rest.chatapi.bean.resp;

import java.util.List;

import com.db.pbc.trxm.rest.chatapi.bean.Message;

import io.swagger.annotations.ApiModelProperty;

public class MessagesJsonResponse {
	@ApiModelProperty(value = "List of messages")
    private List<Message> messages;

    public MessagesJsonResponse(List<Message> messages) {
        this.messages = messages;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> Messages) {
        this.messages = Messages;
    }
}
