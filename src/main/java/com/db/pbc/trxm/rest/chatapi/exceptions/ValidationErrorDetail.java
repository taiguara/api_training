package com.db.pbc.trxm.rest.chatapi.exceptions;

public class ValidationErrorDetail {
    public String validationErrorMessage;
    public String rejectedValue;

    public ValidationErrorDetail() {
    }

    public String getValidationErrorMessage() {
        return validationErrorMessage;
    }

    public void setValidationErrorMessage(String validationError) {
        this.validationErrorMessage = validationError;
    }

    public String getRejectedValue() {
        return rejectedValue;
    }

    public void setRejectedValue(String rejectedValue) {
        this.rejectedValue = rejectedValue;
    }
}