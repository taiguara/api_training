package com.db.pbc.trxm.rest.chatapi.controller.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.db.pbc.trxm.rest.chatapi.exceptions.ApiBaseException;
import com.db.pbc.trxm.rest.chatapi.exceptions.ApiValidationBaseException;
import com.db.pbc.trxm.rest.chatapi.exceptions.ValidationErrorDetail;
import com.db.pbc.trxm.rest.chatapi.utils.ApiLoggerLibrary;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ApiBaseException.class)
    public ResponseEntity<ExceptionJsonResponse> authenticationException(ApiBaseException ex) {
        ApiLoggerLibrary.logError(ex.getMessage(), ex);

        ExceptionJsonResponse exceptionResponse = new ExceptionJsonResponse(ex.getMessage());
        if (ex instanceof ApiValidationBaseException) {
            exceptionResponse.setDetailedErrorMessages(
                    ((ApiValidationBaseException) ex).getDetailedMessages());
        }
        return new ResponseEntity<ExceptionJsonResponse>(exceptionResponse, ex.getCode());
    }

    class ExceptionJsonResponse {
        private String errorMessage;
        private List<ValidationErrorDetailJson> detailedErrorMessages = new ArrayList<>();

        public ExceptionJsonResponse(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public List<ValidationErrorDetailJson> getDetailedErrorMessages() {
            return detailedErrorMessages;
        }

        public void setDetailedErrorMessages(List<ValidationErrorDetail> list) {
            for (ValidationErrorDetail errorDetail : list) {
                ValidationErrorDetailJson errorDetailJson = new ValidationErrorDetailJson();
                errorDetailJson.setValidationErrorMessage(errorDetail.getValidationErrorMessage());
                errorDetailJson.setRejectedValue(errorDetail.getRejectedValue());
                this.detailedErrorMessages.add(errorDetailJson);
            }
        }

        class ValidationErrorDetailJson {
            public String validationErrorMessage;
            public String rejectedValue;

            public String getValidationErrorMessage() {
                return validationErrorMessage;
            }

            public void setValidationErrorMessage(String validationError) {
                this.validationErrorMessage = validationError;
            }

            public String getRejectedValue() {
                return rejectedValue;
            }

            public void setRejectedValue(String rejectedValue) {
                this.rejectedValue = rejectedValue;
            }
        }
    }
}
