package com.db.pbc.trxm.rest.chatapi.repositories;

import java.util.Set;

import com.db.pbc.trxm.rest.chatapi.bean.ChatFile;
import com.db.pbc.trxm.rest.chatapi.bean.ChatRoom;
import com.db.pbc.trxm.rest.chatapi.bean.Message;

public interface IChatRoomRegister {

    int createChatRoom(String name, Boolean isPublic);

    ChatRoom getRoomById(int id);

    Set<ChatRoom> listAllRooms();

    void cleanAllRooms();

    void addMessage(int id, Message message);
    
    int addFile(int id, ChatFile file);

    void deleteChatRoom(int id);

    String getLastMessageContent(int id);
    
    Boolean isPublic(int id);
    
    ChatFile getFile(int roomId, int fileId);
    
    boolean fileExists(int roomId, int fileId);
}
