package com.db.pbc.trxm.rest.chatapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.db.pbc.trxm.rest.chatapi.bean.resp.RegisteredUsersJsonResponse;
import com.db.pbc.trxm.rest.chatapi.exceptions.ApiBaseException;
import com.db.pbc.trxm.rest.chatapi.service.IUserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
public class RegisteredUsersController {

    @Autowired
    private IUserService userService;

    @ApiOperation(
            value = "List the registered users",
            notes = "Return a List of users registerd",
            response = RegisteredUsersJsonResponse.class)
    @RequestMapping(
            value = "/registeredUsers",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<RegisteredUsersJsonResponse> seeRegisteredUsers(
            @ApiParam(value = "Authorization Token", required = true) @RequestHeader(
                    value = "AuthToken") String authToken)
            throws ApiBaseException {

        RegisteredUsersJsonResponse response = new RegisteredUsersJsonResponse()
                .withUsers(userService.listAllUsers());
        return new ResponseEntity<RegisteredUsersJsonResponse>(response, HttpStatus.OK);
    }

}
