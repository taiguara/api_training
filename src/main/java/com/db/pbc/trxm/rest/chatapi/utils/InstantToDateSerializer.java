package com.db.pbc.trxm.rest.chatapi.utils;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;

import org.threeten.bp.Instant;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class InstantToDateSerializer extends JsonSerializer<Instant>{

    @Override
    public void serialize(Instant instant, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {
        
        Date date = new Date(instant.toEpochMilli());
        SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm");
       jsonGenerator.writeString((sdf.format(date)));
        
    }

}
