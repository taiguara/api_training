package com.db.pbc.trxm.rest.chatapi.utils;

import java.util.Properties;

public interface IProperties {
    public void initialize();
    public Properties getConfiguration();
    public String getFilename();
    public String getValue(final String key);
    public boolean getValueAsBoolean(final String key);
    public Integer getValueAsInteger(final String key);
}
