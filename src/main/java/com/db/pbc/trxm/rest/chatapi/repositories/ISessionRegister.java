package com.db.pbc.trxm.rest.chatapi.repositories;

import java.util.List;

import com.db.pbc.trxm.rest.chatapi.bean.Session;

public interface ISessionRegister {

	void addSession(Session session);

	Session getSessionByUserEmail(String email);

	void clearRegister();

	void removeSession(String email);

	Session getSessionByToken(String encodedToken);
	
	List<Session> listAllSessions();
}