package com.db.pbc.trxm.rest.chatapi.service;

import com.db.pbc.trxm.rest.chatapi.bean.Message;

public interface IMessageService {

    void addMessage(int roomId, Message message);
}
