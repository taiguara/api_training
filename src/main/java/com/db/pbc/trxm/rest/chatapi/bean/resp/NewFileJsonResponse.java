package com.db.pbc.trxm.rest.chatapi.bean.resp;

import io.swagger.annotations.ApiModelProperty;

public class NewFileJsonResponse {
    @ApiModelProperty(value = "The File ID in a chat room")
    private int fileId;

    public NewFileJsonResponse(int fileId){
    	this.fileId = fileId;
    }
    
    
    public int getFileId() {
		return fileId;
	}


	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

}