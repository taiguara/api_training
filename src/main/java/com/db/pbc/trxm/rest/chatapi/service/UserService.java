package com.db.pbc.trxm.rest.chatapi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.exceptions.DuplicatedUserException;
import com.db.pbc.trxm.rest.chatapi.exceptions.NoUserProvidedException;
import com.db.pbc.trxm.rest.chatapi.repositories.IUserRegister;

/**
 * Service to manage Users objects.
 *
 * @author OATA
 *
 */
@Service
public class UserService implements IUserService {
    @Autowired
    private IUserRegister userRegister;

    /**
     * Register new user in the system.
     */
    @Override
    public void registerUser(final User user) {
        if (user == null) {
            throw new NoUserProvidedException();
        }
        if (userRegister.getUserByEmail(user.getEmail()) != null) {
            throw new DuplicatedUserException();
        }
        userRegister.addUser(user);
    }

    /**
     * Updates user in the system
     */
    @Override
    public void editUser(String oldMail, final User user) {
        if (user == null) {
            throw new NoUserProvidedException();
        }
        User oldUser = userRegister.getUserByEmail(oldMail);
        userRegister.removeUser(oldUser);
        
        if (userRegister.getUserByEmail(user.getEmail()) != null) {
            throw new DuplicatedUserException();
        }
        userRegister.addUser(user);
    }
    
    /**
     * Find registered user by his email.
     */
    @Override
    public User getUserByEmail(final String userEmail) {
        return userRegister.getUserByEmail(userEmail);
    }

    @Override
    public void clearRegister() {
        userRegister.clearRegister();
    }

    @Override
    public List<User> listAllUsers() {
        return userRegister.listAllUsers();
    }
}