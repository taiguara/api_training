package com.db.pbc.trxm.rest.chatapi.utils;

import org.springframework.stereotype.Component;

@Component("applicationProperties")
public class ApplicationProperties extends AbstractProperties{
    public static final String SESSION_TIMEOUT_IN_SECONDS = "session.timeout.in.seconds";
    public static final String MAX_USERS = "max.users";
    public static final String PROPERTIES_FILE_PATH = System.getProperty("user.home")
            + "/application.properties";
    
    @Override
    public String getFilename() {
        return PROPERTIES_FILE_PATH;
    };
    
}
