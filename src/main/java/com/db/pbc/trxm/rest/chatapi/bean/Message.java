package com.db.pbc.trxm.rest.chatapi.bean;

import java.text.MessageFormat;

import org.threeten.bp.Instant;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import com.db.pbc.trxm.rest.chatapi.utils.InstantToDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.annotations.ApiModelProperty;

public class Message {
    private final String PATTERN = "%s <%s> says at {%s}: \n\t %s";
    @ApiModelProperty(value = "Author of the message")
    private final User author;
    @ApiModelProperty(value = "Content, or the message itself")
    private final String content;
    @ApiModelProperty(value = "Instant that the message was sent")
    @JsonSerialize(using = InstantToDateSerializer.class)
    private final Instant postTime;

    public Message(User author, String content) {
        this.author = author;
        this.content = content;
        this.postTime = Instant.now();
    }

    public User getAuthor() {
        return author;
    }
    public String getContent() {
        return content;
    }

    public Instant getPostTime() {
        return postTime;
    }

    @Override
    public String toString() {
        String timeFormat = DateTimeFormatter.ISO_ZONED_DATE_TIME
                .format(ZonedDateTime.ofInstant(postTime, ZoneId.systemDefault()));
        return MessageFormat.format(PATTERN, author.getName(), author.getEmail(), timeFormat,
                content);
    }
}
