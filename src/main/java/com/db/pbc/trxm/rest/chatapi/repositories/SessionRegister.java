package com.db.pbc.trxm.rest.chatapi.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

import com.db.pbc.trxm.rest.chatapi.bean.Session;

/**
 * Session Register that manages Session objects.
 *
 * @author OATA
 *
 */
@Component
public class SessionRegister implements ISessionRegister {
    /**
     * hashmap to manage the sessions
     */
    private static Map<String, Session> sessions = new ConcurrentHashMap<String, Session>();

    /**
     * add new session.
     */
    @Override
    public void addSession(final Session session) {
        sessions.put(session.getToken(), session);
    }

    /**
     * find session in the session register by the session token.
     *
     * @return: Session found or null if the provided token is not registered in
     *          the system
     */
    @Override
    public Session getSessionByToken(final String token) {
        return sessions.get(token);
    }

    @Override
    public void removeSession(String token) {
        sessions.remove(token);
    }

    /**
     * remove all sessions from session register.
     */
    @Override
    @PreDestroy
    public void clearRegister() {
        sessions.clear();
    }

    /**
     * find session in the session register by the user email.
     *
     * @return: Session found or null if the provided email is not registered in
     *          the system
     */
    @Override
    public Session getSessionByUserEmail(String email) {
        for (Session session : sessions.values()) {
            if (session.getUserEmail().equals(email)) {
                return session;
            }
        }
        return null;
    }

    @Override
    public List<Session> listAllSessions() {
        return new ArrayList<Session>(sessions.values());
    }
}