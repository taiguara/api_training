package com.db.pbc.trxm.rest.chatapi.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApiLoggerLibrary {

    private static final Logger LOGGER = LogManager.getLogger("logError");

    public static void logError(String message, RuntimeException exception) {

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);

        LOGGER.log(Level.ERROR, "|" + message + "\r\n" + sw.toString());
    }
}
