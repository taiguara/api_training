package com.db.pbc.trxm.rest.chatapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db.pbc.trxm.rest.chatapi.bean.Session;
import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.exceptions.NotRegisteredException;

/**
 * Service to manage Session objects at user login.
 *
 * @author OATA
 *
 */
@Service
public class LoginService implements ILoginService {

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private IUserService userService;

    /**
     * Login registered user. Execute login of the user in the system by
     * creating new or renewing the already existing active session
     */
    @Override
    public String login(final String userEmail) {
        final User user = userService.getUserByEmail(userEmail);
        if (user == null) {
            throw new NotRegisteredException();
        } 
        Session session = sessionService.getSessionByUserEmail(userEmail);
        if(session != null){
            sessionService.removeSession(session.getToken());
        }
        session = sessionService.createSession(user.getEmail());
        return session.getToken();
    }

}