package com.db.pbc.trxm.rest.chatapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.db.pbc.trxm.rest.chatapi.exceptions.ApiBaseException;
import com.db.pbc.trxm.rest.chatapi.service.ISessionService;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
public class SessionController {

    @Autowired
    private ISessionService sessionService;

    @ApiOperation(
            value = "Session status",
            notes = "Check if session is active or expired.",
            response = SessionStatusJsonResponse.class)
    @RequestMapping(
            value = "/sessionStatus",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<SessionStatusJsonResponse> sessionStatus(
            @ApiParam(value = "Authorization Token", required = true) @RequestHeader(
                    value = "AuthToken") String authToken)
            throws ApiBaseException {
        SessionStatusJsonResponse response = new SessionStatusJsonResponse();
        if (sessionService.isSessionActive(authToken)) {
            response.setStatus("Active");
        } else {
            response.setStatus("Expired");
        }

        return new ResponseEntity<SessionStatusJsonResponse>(response, HttpStatus.OK);
    }

    class SessionStatusJsonResponse {
        @ApiModelProperty(value = "Status of sesion")
        private String status;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
