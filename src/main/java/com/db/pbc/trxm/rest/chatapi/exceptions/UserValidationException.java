package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

public class UserValidationException extends ApiValidationBaseException {
    private static final long serialVersionUID = 1L;
    private static final String USER_VALIDATION_FAILED_MESSAGE = "User validation Error";

    public UserValidationException() {
        super(HttpStatus.PRECONDITION_FAILED, USER_VALIDATION_FAILED_MESSAGE);
    }
}
