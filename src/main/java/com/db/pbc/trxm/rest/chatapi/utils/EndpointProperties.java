package com.db.pbc.trxm.rest.chatapi.utils;

import org.springframework.stereotype.Component;

@Component("endpointProperties")
public class EndpointProperties extends AbstractProperties {
    public static final String PROPERTIES_FILE_PATH = System.getProperty("user.home")
            + "/endpointconfig.properties";

    @Override
    public String getFilename() {
        return PROPERTIES_FILE_PATH;
    }

}
