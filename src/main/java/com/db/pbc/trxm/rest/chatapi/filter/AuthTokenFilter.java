package com.db.pbc.trxm.rest.chatapi.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import com.db.pbc.trxm.rest.chatapi.bean.resp.JSONResponseMessage;
import com.db.pbc.trxm.rest.chatapi.service.ISessionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AuthTokenFilter extends AbstractApiFilter {

    @Autowired
    ISessionService sessionService;

    @Override
    protected void doInternalFilter(HttpServletRequest req, HttpServletResponse res,
            FilterChain chain) throws JsonProcessingException, IOException, ServletException {
        String token = req.getHeader("AuthToken");
        if (sessionService.isSessionActive(token)) {
            chain.doFilter(req, res);
        } else {
            sessionService.removeSession(token);
            final ObjectMapper mapper = new ObjectMapper();
            JSONResponseMessage responseJson = new JSONResponseMessage("Invalid token or session expired");
            res.getOutputStream().print(
                    mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseJson));
            res.setStatus(HttpStatus.UNAUTHORIZED.value());
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
    @Override
    public void destroy() {

    }
}
