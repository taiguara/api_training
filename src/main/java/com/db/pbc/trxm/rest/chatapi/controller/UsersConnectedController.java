package com.db.pbc.trxm.rest.chatapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.db.pbc.trxm.rest.chatapi.bean.resp.UsersConnectedJsonResponse;
import com.db.pbc.trxm.rest.chatapi.exceptions.ApiBaseException;
import com.db.pbc.trxm.rest.chatapi.service.ISessionService;

import io.swagger.annotations.ApiOperation;

@RestController
public class UsersConnectedController {
    @Autowired
    ISessionService sessionService;

    @ApiOperation(
            value = "List the users connected",
            notes = "List all users connected in all sessions",
            response = UsersConnectedJsonResponse.class)
    @RequestMapping(
            value = "/usersConnected",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<UsersConnectedJsonResponse> usersConnected() throws ApiBaseException {

        UsersConnectedJsonResponse jsonResponse = new UsersConnectedJsonResponse(
                sessionService.listUsersInSession());
        return new ResponseEntity<UsersConnectedJsonResponse>(jsonResponse, HttpStatus.OK);
    }

}
