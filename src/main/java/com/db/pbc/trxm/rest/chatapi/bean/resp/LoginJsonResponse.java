package com.db.pbc.trxm.rest.chatapi.bean.resp;

import io.swagger.annotations.ApiModelProperty;

public class LoginJsonResponse {
    @ApiModelProperty(value = "The session token")
    private String sessionToken;

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public LoginJsonResponse withSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
        return this;
    }
}