package com.db.pbc.trxm.rest.chatapi.bean;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import io.swagger.annotations.ApiModelProperty;

/**
 * user and associated session
 *
 * @author OATA
 *
 */
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "Unique identifier of the user")
    private String email;
    @ApiModelProperty(value = "Person's name")
    private String name;

    public User(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public User() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(getEmail());
        return builder.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof User)) {
            return false;
        }
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(getEmail(), ((User) obj).getEmail());
        return builder.isEquals();
    }

    @Override
    public String toString() {
        return this.email + "#" + this.name;
    }
}
