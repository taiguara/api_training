package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

public class ChatFileNotFoundException extends ApiBaseException {
    private static final long serialVersionUID = 1L;
    private static final String CHAT_FILE_NOT_FOUND = "Chat file for given id was not found!";

    public ChatFileNotFoundException() {
        super(HttpStatus.NOT_FOUND, CHAT_FILE_NOT_FOUND);
    }

}
