package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

public class NoAuthenticationProvidedException extends ApiBaseException {
    private static final long serialVersionUID = 1L;
    private static final String AUTHENTICATION_NOT_PROVIDED_MESSAGE = "User authentication information is not provided";

    public NoAuthenticationProvidedException() {
        super(HttpStatus.BAD_REQUEST, AUTHENTICATION_NOT_PROVIDED_MESSAGE);
    }

}
