package com.db.pbc.trxm.rest.chatapi.service;

import java.util.List;

import com.db.pbc.trxm.rest.chatapi.bean.Session;
import com.db.pbc.trxm.rest.chatapi.bean.User;

public interface ISessionService {

    public Session getSessionByUserEmail(String email);

    public Session getSessionByToken(final String encodedToken);

    Session createSession(final String email);

    boolean isSessionActive(final String encodedToken);

    void clearRegister();

    void removeSession(String encodedToken);
    
    List<User> listUsersInSession();
}
