package com.db.pbc.trxm.rest.chatapi.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;

import com.db.pbc.trxm.rest.chatapi.utils.IProperties;

public class EndpointConfigFilter implements Filter {

    private static final String DEFAULT_DISABLE_MESSAGE = "Endpoint disabled for maintenance or development";
    @Autowired
    @Qualifier("endpointProperties")
    private IProperties properties;

    public EndpointConfigFilter() {
    }

    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        
        if (properties.getConfiguration().containsKey(req.getRequestURI())) {
            boolean enable = properties.getValueAsBoolean(req.getRequestURI());
            if (enable) {
                chain.doFilter(request, response);
            } else {
                res.setStatus(HttpStatus.LOCKED.value());
                res.getOutputStream().println(DEFAULT_DISABLE_MESSAGE);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    public void init(FilterConfig fConfig) throws ServletException {
    }

}
