package com.db.pbc.trxm.rest.chatapi.service;

public interface ILoginService {

    String login(final String userEmail);
}
