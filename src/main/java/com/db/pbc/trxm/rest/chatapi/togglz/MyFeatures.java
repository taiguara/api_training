package com.db.pbc.trxm.rest.chatapi.togglz;

import org.togglz.core.Feature;
import org.togglz.core.annotation.EnabledByDefault;
import org.togglz.core.annotation.Label;
import org.togglz.core.context.FeatureContext;

public enum MyFeatures implements Feature{

    @EnabledByDefault
    @Label("Show last 10 messages feature")
    SHOW_LAST_TEN_MESSAGES,
    @Label("Public/private rooms")
    SET_ROOM_PUBLIC_STATE;
    
    public boolean isActive(){
        return FeatureContext.getFeatureManager().isActive(this);
    }
}
