package com.db.pbc.trxm.rest.chatapi.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;

public abstract class AbstractProperties implements IProperties {
    private Properties configuration = new Properties();

    @PostConstruct
    public void initialize() {
        InputStream in = null;
        try {
            in = new FileInputStream(new File(getFilename()));
            getConfiguration().load(in);
        } catch (IOException e) {
            if (e instanceof FileNotFoundException) {
                //file doesn't exists copy from our resource model (will be inside the jar)
                ClassLoader classLoader = getClass().getClassLoader();
                File filename = new File(getFilename());
                File propertiesFile = new File(
                        classLoader.getResource(filename.getName()).getFile());
                try {
                    FileUtils.copyFile(propertiesFile, filename);
                } catch (IOException e1) {
                    e1.printStackTrace();
                    System.err.println("Properties file " + filename.getName() + " doesn't exists! Please create it!");
                }
                
            } else {
                e.printStackTrace();
            }
        } finally {
            try {
                if (in != null)
                    in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public String getValue(final String key) {
        return (String) getConfiguration().get(key);
    }

    @Override
    public boolean getValueAsBoolean(final String key) {
        return Boolean.valueOf(getValue(key));
    };

    @Override
    public Integer getValueAsInteger(final String key) {
        return Integer.valueOf(getValue(key));
    }

    @Override
    public Properties getConfiguration() {
        return configuration;
    }
}
