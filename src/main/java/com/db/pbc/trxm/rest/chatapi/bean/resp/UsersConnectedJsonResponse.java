package com.db.pbc.trxm.rest.chatapi.bean.resp;

import java.util.List;

import com.db.pbc.trxm.rest.chatapi.bean.User;

import io.swagger.annotations.ApiModelProperty;

public class UsersConnectedJsonResponse {
	@ApiModelProperty(value = "List of all user names connected")
    private List<User> usersConnected;

    public UsersConnectedJsonResponse(List<User> usersConnected) {
        super();
        this.usersConnected = usersConnected;
    }

    public List<User> getUsersConnected() {
        return usersConnected;
    }

    public void setUsersConnected(List<User> usersConnected) {
        this.usersConnected = usersConnected;
    }
}