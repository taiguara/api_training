package com.db.pbc.trxm.rest.chatapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db.pbc.trxm.rest.chatapi.bean.Message;
import com.db.pbc.trxm.rest.chatapi.repositories.IChatRoomRegister;

@Service
public class MessageService implements IMessageService {

    @Autowired
    IChatRoomRegister register;

    @Override
    public void addMessage(int roomId, Message message) {
        register.addMessage(roomId, message);
    }
}
