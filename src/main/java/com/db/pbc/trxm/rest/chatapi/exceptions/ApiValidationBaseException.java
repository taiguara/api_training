package com.db.pbc.trxm.rest.chatapi.exceptions;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

public class ApiValidationBaseException extends ApiBaseException {
    private static final long serialVersionUID = 1L;
    private List<ValidationErrorDetail> detailedErrorMessages = new ArrayList<>();

    public ApiValidationBaseException(HttpStatus code, String message) {
        super(code, message);
    }

    public void addErrorDetail(ObjectError errorDetail) {
        ValidationErrorDetail validationError = new ValidationErrorDetail();
        validationError.setValidationErrorMessage(errorDetail.getDefaultMessage());

        if (errorDetail instanceof FieldError) {
            validationError
                    .setRejectedValue(((FieldError) errorDetail).getRejectedValue().toString());
        }
        detailedErrorMessages.add(validationError);
    }

    public void addErrorDetails(List<ObjectError> errorDetails) {
        for (ObjectError errorDetail : errorDetails) {
            addErrorDetail(errorDetail);
        }
    }

    public ApiValidationBaseException withErrorDetails(List<ObjectError> errorDetails) {
        addErrorDetails(errorDetails);
        return this;
    }

    public List<ValidationErrorDetail> getDetailedMessages() {
        return detailedErrorMessages;

    }
}
