package com.db.pbc.trxm.rest.chatapi.bean.req;

import io.swagger.annotations.ApiModelProperty;

public class MessageJsonRequest {

    @ApiModelProperty(value = "identification", required = true)
    private String id;
    @ApiModelProperty(value = "Message to be sent", required = true)
    private String message;

    public MessageJsonRequest() {

    }

    public MessageJsonRequest(String id, String message) {
        this.id = id;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }
}
