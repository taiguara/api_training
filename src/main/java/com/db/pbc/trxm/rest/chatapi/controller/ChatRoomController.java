package com.db.pbc.trxm.rest.chatapi.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.db.pbc.trxm.rest.chatapi.bean.ChatFile;
import com.db.pbc.trxm.rest.chatapi.bean.ChatRoom;
import com.db.pbc.trxm.rest.chatapi.bean.Message;
import com.db.pbc.trxm.rest.chatapi.bean.Session;
import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.bean.req.MessageJsonRequest;
import com.db.pbc.trxm.rest.chatapi.bean.req.NewRoomJsonRequest;
import com.db.pbc.trxm.rest.chatapi.bean.resp.JSONResponseMessage;
import com.db.pbc.trxm.rest.chatapi.bean.resp.MessagesJsonResponse;
import com.db.pbc.trxm.rest.chatapi.bean.resp.NewFileJsonResponse;
import com.db.pbc.trxm.rest.chatapi.exceptions.ApiBaseException;
import com.db.pbc.trxm.rest.chatapi.exceptions.AuthenticationException;
import com.db.pbc.trxm.rest.chatapi.exceptions.ChatFileNotFoundException;
import com.db.pbc.trxm.rest.chatapi.exceptions.NonExistentRoomException;
import com.db.pbc.trxm.rest.chatapi.service.IChatRoomService;
import com.db.pbc.trxm.rest.chatapi.service.IMessageService;
import com.db.pbc.trxm.rest.chatapi.service.ISessionService;
import com.db.pbc.trxm.rest.chatapi.service.IUserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
public class ChatRoomController {
    @Autowired
    private IChatRoomService chatRoomService;
    @Autowired
    private IMessageService messageService;
    @Autowired
    private ISessionService sessionService;
    @Autowired
    private IUserService userService;

    @ApiOperation(
            value = "Return messages from a specific room",
            notes = "Return messages from a room identified by id",
            response = MessagesJsonResponse.class)
    @RequestMapping(
            value = "/chatRoom/messages",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MessagesJsonResponse> messagesFromRoom(
            @ApiParam(
                    value = "identification of the room to retrieve the messages",
                    required = true) @RequestParam final int id,
            @ApiParam(value = "Authorization Token", required = true) @RequestHeader(
                    value = "AuthToken") String authToken)
            throws ApiBaseException {
        ChatRoom chatRoom = null;
        
        if (chatRoomService.roomExists(id)){
             chatRoom = chatRoomService.getRoomById(id);
        } else {
            throw new NonExistentRoomException();
        }
        
        if (!chatRoom.isPublic()){
            if (!sessionService.isSessionActive(authToken)){
                throw new AuthenticationException();
            }
        }
        
        List<Message> messagesFromRoom = chatRoomService.getMessagesFromRoom(id);

        MessagesJsonResponse jsonResponse = new MessagesJsonResponse(messagesFromRoom);
        return new ResponseEntity<MessagesJsonResponse>(jsonResponse, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Add a new message to a specific room",
            notes = "Add a new message to a room identified by id")
    @RequestMapping(
            value = "/chatRoom/Message",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> addNewMessage(
            @ApiParam(
                    value = "Message request",
                    required = true) @RequestBody final MessageJsonRequest messageRequest,
            @ApiParam(value = "Authorization token", required = true) @RequestHeader(
                    value = "AuthToken") String authToken) throws JsonProcessingException {
                User user = null;
        int roomId = Integer.valueOf(messageRequest.getId());
        if (!chatRoomService.roomExists(roomId)){
            throw new NonExistentRoomException();
        }
        if (!sessionService.isSessionActive(authToken)){
            user = new User("", "Anonymous");
        } else {
            user = userService.getUserByEmail(sessionService.getSessionByToken(authToken).getUserEmail());
        }
        ChatRoom chatRoom = chatRoomService.getRoomById(roomId);
        
        if (!chatRoom.isPublic()) {
            if (!sessionService.isSessionActive(authToken)) {

                sessionService.removeSession(authToken);
                final ObjectMapper mapper = new ObjectMapper();
                JSONResponseMessage responseJson = new JSONResponseMessage("Invalid token or session expired");
                return new ResponseEntity<String>(
                        mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseJson),
                        HttpStatus.BAD_REQUEST);
            } else {
                Session session = sessionService.getSessionByToken(authToken);
                user = userService.getUserByEmail(session.getUserEmail());
            }
        }

        List<Message> messagesFromRoom = chatRoomService.getMessagesFromRoom(roomId);
        if (messagesFromRoom == null) {
            throw new NonExistentRoomException();
        }
        Message message = new Message(user, messageRequest.getMessage());
        messageService.addMessage(roomId, message);

        return new ResponseEntity<String>(HttpStatus.CREATED);
    }
    
    private JSONResponseMessage performSemanticValidation(int roomId, User user, String authToken){
    	JSONResponseMessage responseJson = null;
    	
        if (!chatRoomService.roomExists(roomId)){
            throw new NonExistentRoomException();
        }
        if (!sessionService.isSessionActive(authToken)){
            user = new User("", "Anonymous");
        } else {
            user = userService.getUserByEmail(sessionService.getSessionByToken(authToken).getUserEmail());
        }
        ChatRoom chatRoom = chatRoomService.getRoomById(roomId);
        
        if (!chatRoom.isPublic()) {
            if (!sessionService.isSessionActive(authToken)) {

                sessionService.removeSession(authToken);
                responseJson = new JSONResponseMessage("Invalid token or session expired");
            } else {
                Session session = sessionService.getSessionByToken(authToken);
                user = userService.getUserByEmail(session.getUserEmail());
            }
        }
        return responseJson;
    }
    
    private int storeFile(MultipartFile fileUploaded, int roomId, User user) throws IOException{
        byte[] bytes = new byte[ChatFile.MAX_UPLOAD_SIZE];
        bytes = fileUploaded.getBytes();

        ChatFile file = new ChatFile(user, bytes, fileUploaded.getName());
        int fileID = chatRoomService.addFile(roomId, file);
        
        return fileID;
    }
    
    @ApiOperation(
            value = "Add a new file to a specific room",
            notes = "Add a new file to a room identified by id")
    @RequestMapping(
            value = "/chatRoom/File",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> addNewFile(
            @ApiParam(
                    value = "Chat ID",
                    required = true) @RequestParam("roomId") String roomIdReq,
            @ApiParam(
                    value = "File",
                    required = true) @RequestParam("file") MultipartFile fileUploaded,
            @ApiParam(value = "Authorization token", required = true) @RequestHeader(
                    value = "AuthToken") String authToken) throws JsonProcessingException, IOException {
                User user = null;
        int roomId = Integer.valueOf(roomIdReq);
        final ObjectMapper mapper = new ObjectMapper();
        
        JSONResponseMessage responseJson = performSemanticValidation(roomId, user, authToken);
        
        if (responseJson != null)
        	return new ResponseEntity<String>(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseJson),HttpStatus.BAD_REQUEST);
        
        NewFileJsonResponse response = new NewFileJsonResponse(storeFile(fileUploaded, roomId, user));

        return new ResponseEntity<String>(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response),HttpStatus.CREATED);
    }
    
    @ApiOperation(
            value = "Add a new room",
            notes = "Adda new room to the chat")
    @RequestMapping(
            value = "/Room",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> addNewRoom(@ApiParam(
            value = "request for creating the new room",
            required = true) @RequestBody final NewRoomJsonRequest messageRequest) {
        String roomName = messageRequest.getRoomName();
        Boolean isPublic = messageRequest.isPublicRoom();

        chatRoomService.createChatRoom(roomName, isPublic);

        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
   
    @ApiOperation(
            value = "Retrieve a file from a specific room",
            notes = "Retrieve a file previously sent to a chat room.")
    @RequestMapping(
            value = "/chatRoom/File",
            method = RequestMethod.GET)
    public ResponseEntity<ByteArrayResource> getChatFile(
            @ApiParam(
                    value = "room id from where to get the file",
                    required = true) @RequestParam int roomId,
            @ApiParam(
                    value = "the file id to be retrieved",
                    required = true) @RequestParam int fileId,
            @ApiParam(value = "Authorization Token", required = true) @RequestHeader(
                    value = "AuthToken") String authToken) {

        performSemanticValidation(roomId, fileId);
        
        ResponseEntity<ByteArrayResource> result = retrieveByteArrayResourceFromFile(roomId,
                fileId);
        return result;
    }

    private ResponseEntity<ByteArrayResource> retrieveByteArrayResourceFromFile(int roomId,
            int fileId) {
        ChatFile file = chatRoomService.getFile(roomId, fileId);

        ByteArrayResource resource = new ByteArrayResource(file.getFileContent());

        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        respHeaders.setContentLength(resource.contentLength());
        respHeaders.setContentDispositionFormData("attachment", file.getFileName());

        ResponseEntity<ByteArrayResource> result = new ResponseEntity<ByteArrayResource>(resource,
                respHeaders, HttpStatus.OK);
        return result;
    }

    private void performSemanticValidation(int roomId, int fileId) {
        if (!chatRoomService.roomExists(roomId)) {
            throw new NonExistentRoomException();
        }
        if (!chatRoomService.fileExists(roomId, fileId)) {
            throw new ChatFileNotFoundException();
        }
    }
}
