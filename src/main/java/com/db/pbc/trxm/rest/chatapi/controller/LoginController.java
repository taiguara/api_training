package com.db.pbc.trxm.rest.chatapi.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.db.pbc.trxm.rest.chatapi.bean.req.LoginJsonRequest;
import com.db.pbc.trxm.rest.chatapi.bean.resp.LoginJsonResponse;
import com.db.pbc.trxm.rest.chatapi.exceptions.ApiBaseException;
import com.db.pbc.trxm.rest.chatapi.exceptions.NotRegisteredException;
import com.db.pbc.trxm.rest.chatapi.exceptions.UserValidationException;
import com.db.pbc.trxm.rest.chatapi.service.ILoginService;
import com.db.pbc.trxm.rest.chatapi.service.IUserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
public class LoginController {
    @Autowired
    private ILoginService loginService;
    @Autowired
    private IUserService userService;

    @ApiOperation(
            value = "Sign in registered user",
            notes = "Sign in of the user in the system by creating new or renewing the already existing active session",
            response = LoginJsonResponse.class)
    @RequestMapping(
            value = "/login",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<LoginJsonResponse> userLogin(
            @ApiParam(
                    value = "Request fields necessary to login",
                    required = true) @Valid @RequestBody final LoginJsonRequest loginUserParam,
            BindingResult bindingResult) throws ApiBaseException {
        // validation results
        if (bindingResult.hasErrors()) {
            throw new UserValidationException().withErrorDetails(bindingResult.getAllErrors());
        }

        if (userService.getUserByEmail(loginUserParam.getEmail()) == null) {
            throw new NotRegisteredException();
        }

        String sessionToken = loginService.login(loginUserParam.getEmail());
        LoginJsonResponse response = new LoginJsonResponse().withSessionToken(sessionToken);
        return new ResponseEntity<LoginJsonResponse>(response, HttpStatus.OK);
    }
}
