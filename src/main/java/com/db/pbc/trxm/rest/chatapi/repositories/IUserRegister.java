package com.db.pbc.trxm.rest.chatapi.repositories;

import java.util.List;

import com.db.pbc.trxm.rest.chatapi.bean.User;

public interface IUserRegister {

    void addUser(User user);
    void removeUser(User user);
    User getUserByEmail(String email);
    List<User> listAllUsers();
    void clearRegister();
}