package com.db.pbc.trxm.rest.chatapi.bean.req;

import io.swagger.annotations.ApiModelProperty;

public class NewRoomJsonRequest {

	@ApiModelProperty(value = "Room name", required = true)
	private String roomName;
    
	@ApiModelProperty(value = "True if the room is public", required = true)
    private Boolean publicRoom;

    public NewRoomJsonRequest() {

    }

    public NewRoomJsonRequest(String roomName, Boolean publicRoom) {
        this.roomName = roomName;
        this.publicRoom = publicRoom;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Boolean isPublicRoom() {
        return publicRoom;
    }

    public void setPublicRoom(Boolean publicRoom) {
        this.publicRoom = publicRoom;
    }
    
    
}
