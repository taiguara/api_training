package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Exception in case of trying to userRegister a user with an existing email.
 *
 * @author OATA
 *
 */
public class DuplicatedUserException extends ApiBaseException {
    private static final long serialVersionUID = 1L;
    private static final String DUPLICATE_USER_MESSAGE = "User registration failed. User with provided email already exists";

    public DuplicatedUserException() {
        super(HttpStatus.CONFLICT, DUPLICATE_USER_MESSAGE);
    }
}
