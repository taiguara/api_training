package com.db.pbc.trxm.rest.chatapi.service;

import java.util.List;

import com.db.pbc.trxm.rest.chatapi.bean.User;

public interface IUserService {
    void registerUser(User user);
    void editUser(String oldMail, User user);
    User getUserByEmail(String userEmail);
    List<User> listAllUsers();
    void clearRegister();
}
