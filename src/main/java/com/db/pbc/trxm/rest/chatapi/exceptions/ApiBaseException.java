package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Exception with the associated HTTP Status code.
 * 
 * @author OATA
 *
 */
public class ApiBaseException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    /**
     * httpstatus associated with the business exception
     */
    private HttpStatus code;

    public ApiBaseException(final HttpStatus code, final String message) {
        super(message);
        this.code = code;
    }

    public HttpStatus getCode() {
        return code;
    }

    public void setCode(final HttpStatus code) {
        this.code = code;
    }
}
