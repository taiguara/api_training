package com.db.pbc.trxm.rest.chatapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db.pbc.trxm.rest.chatapi.bean.ChatFile;
import com.db.pbc.trxm.rest.chatapi.bean.ChatRoom;
import com.db.pbc.trxm.rest.chatapi.bean.Message;
import com.db.pbc.trxm.rest.chatapi.repositories.IChatRoomRegister;
import com.db.pbc.trxm.rest.chatapi.togglz.MyFeatures;

@Service
public class ChatRoomService implements IChatRoomService {

    @Autowired
    IChatRoomRegister register;

    @Override
    public List<Message> getMessagesFromRoom(int id) {
        ChatRoom room = register.getRoomById(id);
        if (room == null) {
            return null;
        }
        if (MyFeatures.SHOW_LAST_TEN_MESSAGES.isActive()){
           
            List<Message> messages = room.getMessages();
            List<Message> recentMessages = new ArrayList<Message>();
            Integer size = messages.size();
            if (size <= 10 && size > 0){
                for (int i = 0; i < size; i++){
                    recentMessages.add(messages.get(i));
                }
            } else if (size > 10){
                for (int i = size - 10; i < size; i++){
                    recentMessages.add(messages.get(i));
                }
            }
            
            return recentMessages;
        }
        return room.getMessages();
    }

    @Override
    public int createChatRoom(String name, Boolean isPublic) {
        return register.createChatRoom(name, isPublic);
    }

    @Override
    public void deleteChatRoomById(int id) {
        register.deleteChatRoom(id);
    }

    @Override
    public ChatRoom getRoomById(int id) {
        return register.getRoomById(id);
    }

    @Override
    public Set<ChatRoom> listAllRooms() {
        return register.listAllRooms();
    }

    @Override
    public void cleanAllRooms() {
        register.cleanAllRooms();
    }

    @Override
    public String getLastMessageContent(int id) {
        return register.getLastMessageContent(id);
    }

    @Override
    public Boolean roomExists(int id) {
        if (getRoomById(id) != null)
            return true;
        else
            return false;
    }

    @Override
    public Boolean isPublic(int id) {
        return register.isPublic(id);
    }

    @Override
    public int addFile(int roomId, ChatFile file) {
        int fileID = register.addFile(roomId, file);
        return fileID;
    }

    @Override
    public ChatFile getFile(int roomId, int fileId) {
        ChatFile file = register.getFile(roomId, fileId);
        return file;
    }

    @Override
    public boolean fileExists(int roomId, int fileId) {
        return register.fileExists(roomId, fileId);
    }

}
