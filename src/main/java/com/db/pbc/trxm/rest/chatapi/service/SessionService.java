package com.db.pbc.trxm.rest.chatapi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.db.pbc.trxm.rest.chatapi.bean.Session;
import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.exceptions.ChatIsFullException;
import com.db.pbc.trxm.rest.chatapi.repositories.ISessionRegister;
import com.db.pbc.trxm.rest.chatapi.repositories.IUserRegister;
import com.db.pbc.trxm.rest.chatapi.utils.ApplicationProperties;
import com.db.pbc.trxm.rest.chatapi.utils.IProperties;

/**
 * Service Session objects.
 *
 * @author OATA
 *
 */
@Service
public class SessionService implements ISessionService {

    @Autowired
    private ISessionRegister register;
    @Autowired
    private IUserRegister userRegister;
    @Autowired
    @Qualifier("applicationProperties")
    private IProperties properties;
    /**
     * Create or renew user session.
     *
     * @param user
     *            - registered user
     * @return Session associated with the user
     */

    @Override
    public Session createSession(final String email) {
        int maxUsers = properties.getValueAsInteger(ApplicationProperties.MAX_USERS);
        int totalUsers = listUsersInSession().size();
        
        // check if chat is full
        if (totalUsers >= maxUsers) {
            //chat is full
            throw new ChatIsFullException();
        }
        
        Session session = register.getSessionByUserEmail(email);
        if (session == null || session.hasExpired()) {
                int sessionTimeout = properties.getValueAsInteger(ApplicationProperties.SESSION_TIMEOUT_IN_SECONDS);
                session = new Session(email, sessionTimeout);
                register.addSession(session);
        } else {
            session.renew();
        }
        return session;
    }

    public SessionService() {
        super();
    }

    @Override
    public boolean isSessionActive(final String encodedToken) {
        Session session = null;
        
        if (encodedToken != null) {
            session = getSessionByToken(encodedToken);
        }
        if (session == null) {
            return false;
        }
        if (session.hasExpired()) {
            register.removeSession(encodedToken);
            return false;
        }

        return session.getToken().equals(encodedToken);
    }

    @Override
    public Session getSessionByUserEmail(String email) {
        return register.getSessionByUserEmail(email);
    }

    @Override
    public Session getSessionByToken(final String encodedToken) {
        return register.getSessionByToken(encodedToken);
    }

    @Override
    public void removeSession(String encodedToken) {
        if (encodedToken != null){
            register.removeSession(encodedToken);
        }
    }

    @Override
    public void clearRegister() {
        register.clearRegister();
    }

    @Override
    public List<User> listUsersInSession() {
        List<Session> sessions = register.listAllSessions();
        List<User> usersInSession = new ArrayList<User>();
        for (Session session : sessions) {
            usersInSession.add(userRegister.getUserByEmail(session.getUserEmail()));
        }
        return usersInSession;
    }
}
