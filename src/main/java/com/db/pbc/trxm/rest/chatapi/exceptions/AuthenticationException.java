package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

/**
 * exception in case of authentication error.
 *
 * @author OATA
 *
 */
public class AuthenticationException extends ApiBaseException {
    private static final long serialVersionUID = 1L;
    private static final String AUTHENTICATION_FAILED_MESSAGE = "Authentication Error. User session could be expired";

    public AuthenticationException() {
        super(HttpStatus.UNAUTHORIZED, AUTHENTICATION_FAILED_MESSAGE);
    }
}
