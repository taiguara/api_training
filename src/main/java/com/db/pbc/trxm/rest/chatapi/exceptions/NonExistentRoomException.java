package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

public class NonExistentRoomException extends ApiBaseException {
    private static final String EXCEPTION_MESSAGE = "The given chat room is nonexistent.";
    private static final long serialVersionUID = 1L;

    public NonExistentRoomException() {
        super(HttpStatus.NOT_FOUND, EXCEPTION_MESSAGE);
    }
}
