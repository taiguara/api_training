package com.db.pbc.trxm.rest.chatapi.service;

import java.util.List;
import java.util.Set;

import com.db.pbc.trxm.rest.chatapi.bean.ChatFile;
import com.db.pbc.trxm.rest.chatapi.bean.ChatRoom;
import com.db.pbc.trxm.rest.chatapi.bean.Message;

public interface IChatRoomService {

    List<Message> getMessagesFromRoom(int id);

    int createChatRoom(String name, Boolean isPublic);

    void deleteChatRoomById(int id);

    ChatRoom getRoomById(int id);

    Set<ChatRoom> listAllRooms();

    void cleanAllRooms();

    String getLastMessageContent(int id);

    Boolean roomExists(int id);

    Boolean isPublic(int id);

    int addFile(int roomId, ChatFile file);
    
    ChatFile getFile(int roomId, int fileId);
    
    boolean fileExists(int roomId, int fileId);
}
