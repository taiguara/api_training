package com.db.pbc.trxm.rest.chatapi.bean;

import io.swagger.annotations.ApiModelProperty;

import java.text.MessageFormat;

import org.threeten.bp.Instant;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import com.db.pbc.trxm.rest.chatapi.utils.InstantToDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class ChatFile {
	public static final int MAX_UPLOAD_SIZE = 1000;
	private final String PATTERN = "%s <%s> sends file at {%s}: \n\t Name: %s";
    @ApiModelProperty(value = "File sender")
    private final User sender;
    @ApiModelProperty(value = "File content in bytes")
    private final byte[] fileContent;
    @ApiModelProperty(value = "File Name")
    private final String fileName;
    @ApiModelProperty(value = "Instant that the file was sent")
    @JsonSerialize(using = InstantToDateSerializer.class)
    private final Instant postTime;

    public ChatFile(User sender, byte[] fileContent, String fileName) {
        this.sender = sender;
        this.fileContent = fileContent;
        this.fileName = fileName;
        this.postTime = Instant.now();
    }

    public User getSender() {
        return sender;
    }

    public String getFileName() {
        return fileName;
    }

    public byte[] getFileContent() {
        return fileContent;
    }

    public Instant getPostTime() {
        return postTime;
    }

    @Override
    public String toString() {
        String timeFormat = DateTimeFormatter.ISO_ZONED_DATE_TIME
                .format(ZonedDateTime.ofInstant(postTime, ZoneId.systemDefault()));
        return MessageFormat.format(PATTERN, sender.getName(), sender.getEmail(), timeFormat,
        		fileName);
    }
}
