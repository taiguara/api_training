package com.db.pbc.trxm.rest.chatapi.bean;

import java.io.Serializable;
import java.util.UUID;

import org.threeten.bp.Instant;

import com.db.pbc.trxm.rest.chatapi.utils.ChatLibrary;

/**
 * Session object associated with the user and managed by the server.
 *
 * @author OATA
 *
 */
public class Session implements Serializable {
    private static final long serialVersionUID = 1L;
    // session token - identifies current session
    private String token;
    // set the expiration time for the session in case of inactivity
    private Instant expirationTime;
    private Instant creationTime;
    // associated user email as user identifier
    private String userEmail;
    // session timeout in seconds
    private int sessionTimeout;
    
    public Session() {
    }

    /**
     *
     * @param userEmail
     *            - User email registered in the system.
     */
    public Session(final String userEmail, int sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
        this.userEmail = userEmail;
        this.creationTime = Instant.now();
        setNewExpireTime();
        this.token = ChatLibrary
                .encodeString(UUID.randomUUID().toString() + ":" + this.creationTime);
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String sessionId) {
        this.token = sessionId;
    }

    public Instant getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(final Instant expirationTime) {
        this.expirationTime = expirationTime;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(final Instant creationTime) {
        this.creationTime = creationTime;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(final String userEmail) {
        this.userEmail = userEmail;
    }

    public boolean hasExpired() {
        return this.expirationTime != null && this.expirationTime.isBefore(Instant.now());
    }

    /**
     * Renew the session. renew the expiration date by extending it by default
     * expiration interval if the user made an action while the session is still
     * active
     */
    public void renew() {
        if (!hasExpired()) {
            setNewExpireTime();
        }
    }

    private void setNewExpireTime() {
        this.expirationTime = Instant.now().plusSeconds(sessionTimeout);
    }
}
