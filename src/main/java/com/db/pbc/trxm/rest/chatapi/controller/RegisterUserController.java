package com.db.pbc.trxm.rest.chatapi.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.db.pbc.trxm.rest.chatapi.bean.Session;
import com.db.pbc.trxm.rest.chatapi.bean.User;
import com.db.pbc.trxm.rest.chatapi.bean.req.RegisterUserJsonRequest;
import com.db.pbc.trxm.rest.chatapi.exceptions.ApiBaseException;
import com.db.pbc.trxm.rest.chatapi.exceptions.UserValidationException;
import com.db.pbc.trxm.rest.chatapi.service.ISessionService;
import com.db.pbc.trxm.rest.chatapi.service.IUserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
public class RegisterUserController {
    @Autowired
    private IUserService userService;
    @Autowired
    private ISessionService sessionService;

    @ApiOperation(
            value = "Register new user in the system.",
            notes = "Register new user in the system. No duplicate users allowed!")
    @RequestMapping(
            value = "/registerUser",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> registerUser(
            @ApiParam(
                    value = "Request necessary to register the user",
                    required = true) @Valid @RequestBody final RegisterUserJsonRequest registerUserParam,
            BindingResult bindingResult) throws ApiBaseException {

        // validation results
        if (bindingResult.hasErrors()) {
            UserValidationException validationError = new UserValidationException();
            validationError.addErrorDetails(bindingResult.getAllErrors());
            throw validationError;
        }

        User user = mapJsonResponeToUser(registerUserParam);
        userService.registerUser(user);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
    
    @RequestMapping(
            value = "/User",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Void> editUser(
            @Valid @RequestBody final RegisterUserJsonRequest updateUserParam,
            @RequestHeader(value = "AuthToken") String token,
            BindingResult bindingResult) throws ApiBaseException {
        
        Session session = sessionService.getSessionByToken(token);
        String oldMail = session.getUserEmail();
        
        User updatedUser = new User(updateUserParam.getEmail(),
                updateUserParam.getName());
        
        userService.editUser(oldMail, updatedUser);
        session.setUserEmail(updateUserParam.getEmail());
        
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    private User mapJsonResponeToUser(RegisterUserJsonRequest requestParam) {
        User user = new User();
        user.setEmail(requestParam.getEmail());
        user.setName(requestParam.getName());
        return user;
    }
}