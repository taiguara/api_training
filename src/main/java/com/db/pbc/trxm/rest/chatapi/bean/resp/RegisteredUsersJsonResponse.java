package com.db.pbc.trxm.rest.chatapi.bean.resp;

import java.util.List;

import com.db.pbc.trxm.rest.chatapi.bean.User;

import io.swagger.annotations.ApiModelProperty;

public  class RegisteredUsersJsonResponse {
	@ApiModelProperty(value = "List of all users registered")
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public RegisteredUsersJsonResponse withUsers(List<User> users) {
        this.users = users;
        return this;
    }
}