package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Exception in case of a non provided user.
 *
 * @author OATA
 *
 */
public class NoUserProvidedException extends ApiBaseException {
    private static final long serialVersionUID = 1L;
    private static final String USER_NOT_PROVIDED_MESSAGE = "User not provided";

    public NoUserProvidedException() {
        super(HttpStatus.BAD_REQUEST, USER_NOT_PROVIDED_MESSAGE);
    }

}
