package com.db.pbc.trxm.rest.chatapi.togglz;

import java.io.File;

import org.springframework.stereotype.Component;
import org.togglz.core.Feature;
import org.togglz.core.manager.TogglzConfig;
import org.togglz.core.repository.StateRepository;
import org.togglz.core.repository.file.FileBasedStateRepository;
import org.togglz.core.user.FeatureUser;
import org.togglz.core.user.SimpleFeatureUser;
import org.togglz.core.user.UserProvider;

@Component
public class TogglzConfiguration implements TogglzConfig {
    
    public Class<? extends Feature> getFeatureClass() {
        return MyFeatures.class;
    }

    public StateRepository getStateRepository() {
        ClassLoader classLoader = getClass().getClassLoader();
        return new FileBasedStateRepository(new File(classLoader.getResource("features.properties").getFile()));
    }

    @Override
    public UserProvider getUserProvider() {

        return new UserProvider() {
            @Override
            public FeatureUser getCurrentUser() {
                return new SimpleFeatureUser("admin", true);
            }
        }; 
    }
}
