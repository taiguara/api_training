package com.db.pbc.trxm.rest.chatapi.bean;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class ChatRoom {
    private int id;
    private String name;
    private List<Message> messages = new ArrayList<Message>();
    private List<ChatFile> files = new ArrayList<ChatFile>();
    private boolean isPublic;

    public ChatRoom(ChatRoom chatRoom) {
        this.id = chatRoom.getId();
        this.name = chatRoom.getName();
        this.messages = chatRoom.getMessages();
        this.files = chatRoom.getFiles();
        this.isPublic = chatRoom.isPublic();
    }

    public ChatRoom() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public List<ChatFile> getFiles() {
        return files;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(getName());
        return builder.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ChatRoom)) {
            return false;
        }
        EqualsBuilder builder = new EqualsBuilder();
        builder.append(getName(), ((ChatRoom) obj).getName());
        return builder.isEquals();
    }

    @Override
    public String toString() {
        StringBuilder stringFormat = new StringBuilder();
        stringFormat.append("ID: " + id + ". ChatRoom: " + name + "\n");
        for (Message message : messages) {
            stringFormat.append(message + "\n");
        }
        for (ChatFile file : files) {
            stringFormat.append(file + "\n");
        }
        return stringFormat.toString();
    }

}
