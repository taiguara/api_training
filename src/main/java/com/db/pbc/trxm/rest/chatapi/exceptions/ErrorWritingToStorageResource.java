package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

public class ErrorWritingToStorageResource extends ApiBaseException {

    private static final long serialVersionUID = 1L;
    private static final String ERROR_READING_STORAGE_MESSAGE = "Failed reading storage resource";

    public ErrorWritingToStorageResource() {
        super(HttpStatus.INTERNAL_SERVER_ERROR, ERROR_READING_STORAGE_MESSAGE);
    }
}
