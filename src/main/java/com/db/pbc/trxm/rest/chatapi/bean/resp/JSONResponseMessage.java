package com.db.pbc.trxm.rest.chatapi.bean.resp;

import io.swagger.annotations.ApiModelProperty;

public class JSONResponseMessage {
	@ApiModelProperty( value = "Response message", required = true )
    String message;

    public JSONResponseMessage(String messagen) {
        this.message = messagen;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}
