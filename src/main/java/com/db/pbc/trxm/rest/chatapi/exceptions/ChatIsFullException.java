package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

public class ChatIsFullException extends ApiBaseException {
    private static final long serialVersionUID = 1L;
    private static final String CHAR_IS_FULL = "Chat system is full, try again later";

    public ChatIsFullException() {
        super(HttpStatus.LOCKED, CHAR_IS_FULL);
    }

}
