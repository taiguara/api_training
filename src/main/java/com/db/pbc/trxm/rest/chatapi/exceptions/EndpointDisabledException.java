package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

public class EndpointDisabledException extends ApiBaseException {
    private static final long serialVersionUID = 1L;
    private static final String ENDPOINT_DISLABLED= "Endpoint disabled for maintenance or development";

    public EndpointDisabledException() {
        super(HttpStatus.LOCKED, ENDPOINT_DISLABLED);
    }
}
