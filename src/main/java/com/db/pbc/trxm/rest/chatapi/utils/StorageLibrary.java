package com.db.pbc.trxm.rest.chatapi.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.core.io.Resource;

import com.db.pbc.trxm.rest.chatapi.bean.User;

public class StorageLibrary {

    public static void loadUsersFromStorage(Resource storage, Map<String, User> users)
            throws IOException {
        InputStream is = storage.getInputStream();
        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(is));

        String strUser = bufferReader.readLine();
        while (strUser != null) {
            final String[] dataUser = strUser.split(Pattern.quote("#"));
            User user = new User(dataUser[0], dataUser[1]);
            users.put(user.getEmail(), user);
            strUser = bufferReader.readLine();
        }
        bufferReader.close();
        is.close();
    }

    public static void appendUserData(Resource storage, User user) throws IOException {
        FileWriter fw = new FileWriter(storage.getFile(), true);
        BufferedWriter bufferWriter = new BufferedWriter(fw);
        bufferWriter.append(user.toString());
        bufferWriter.newLine();
        bufferWriter.close();
        fw.close();
    }
    
    public static void removeUserData(Resource storage, Map<String, User> users, String removeEmail) throws IOException{
        InputStream is = storage.getInputStream();
        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(is));
        
        String strUser = bufferReader.readLine();
        while (strUser != null) {
            final String[] dataUser = strUser.split(Pattern.quote("#"));
            User user = new User(dataUser[0], dataUser[1]);
            if (!removeEmail.equalsIgnoreCase(user.getEmail())){
                users.put(user.getEmail(), user);
            }
            strUser = bufferReader.readLine();
        }
        bufferReader.close();
        is.close();
    }
    
}
