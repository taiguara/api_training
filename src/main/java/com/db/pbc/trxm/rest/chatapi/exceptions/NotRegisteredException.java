package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Exception in case of login of a user that was not userRegister before.
 *
 * @author OATA
 *
 */
public class NotRegisteredException extends ApiBaseException {
    private static final long serialVersionUID = 1L;
    private static final String NO_EMAIL_FOUND_MESSAGE = "Login Error. User with provided email is not registered";

    public NotRegisteredException() {
        super(HttpStatus.UNAUTHORIZED, NO_EMAIL_FOUND_MESSAGE);
    }
}
