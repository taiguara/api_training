package com.db.pbc.trxm.rest.chatapi.bean.req;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

public class RegisterUserJsonRequest {
    @ApiModelProperty( value = "Person's e-mail address", required = true )
    @Email(message = "Email format is wrong")
    @NotEmpty(message = "User email can not be empty")
    public String email;
    @ApiModelProperty( value = "User name", required = true )
    @Size(min = 2, max = 32, message = "User name length should be from 2 to 32")
    @NotEmpty(message = "User Name is mandatory")
    public String name;

    public RegisterUserJsonRequest(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public RegisterUserJsonRequest() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
