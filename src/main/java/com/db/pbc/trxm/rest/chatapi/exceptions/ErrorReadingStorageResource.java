package com.db.pbc.trxm.rest.chatapi.exceptions;

import org.springframework.http.HttpStatus;

public class ErrorReadingStorageResource extends ApiBaseException {

    private static final long serialVersionUID = 1L;
    private static final String ERROR_WRITING_TO_STORAGE_MESSAGE = "Failed writing to storage resource";

    public ErrorReadingStorageResource() {
        super(HttpStatus.INTERNAL_SERVER_ERROR, ERROR_WRITING_TO_STORAGE_MESSAGE);
    }
}
