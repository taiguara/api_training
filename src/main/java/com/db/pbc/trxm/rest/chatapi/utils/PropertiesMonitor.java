package com.db.pbc.trxm.rest.chatapi.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class PropertiesMonitor {
    private WatchService watcher;
    private Map<String, IProperties> propertiesList;
    private Collection<WatchKey> keys;

    private void register(IProperties prop) throws IOException {
        Path dir = new File(prop.getFilename()).toPath();
        WatchKey key = dir.getParent().register(watcher, StandardWatchEventKinds.ENTRY_MODIFY);
        keys.add(key);
        propertiesList.put(dir.getFileName().toString(), prop);
    }

    public PropertiesMonitor(IProperties... properties) {
        try {
            this.watcher = FileSystems.getDefault().newWatchService();
            this.keys = new HashSet<>();
            this.propertiesList = new HashMap<String, IProperties>();
            for (final IProperties prop : properties) {
                File configFile = new File(prop.getFilename());
                System.out.println("file being monitored: " + configFile);

                register(prop);
            }

            TimerTask task = new TimerTask() {

                @Override
                public void run() {
                    processEvents();
                }
            };
            new Timer().schedule(task, new Date(), 1500);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process all events for keys queued to the watcher
     * 
     * @param prop
     */
    void processEvents() {

        // wait for key to be signaled
        WatchKey key;
        try {
            key = watcher.take();
        } catch (InterruptedException x) {
            x.printStackTrace();
            return;
        }

        // check if currently key is being monitored by us
        if (!keys.contains(key)) {
            return;
        }
        
        for (WatchEvent<?> event : key.pollEvents()) {
            if (event.kind() == (StandardWatchEventKinds.ENTRY_MODIFY)) {
                String filename = event.context().toString();
                IProperties prop = propertiesList.get(filename);
                if (prop == null) {
                    // file not being monitored
                    continue;
                }
                prop.initialize();
            }
        }
        key.reset();
    }

}
